package com.check.base;

// 统一返回结果码
public enum ResponseCode {
    // 公共访问请求类
    FAIL(0,"请求失败"),
    SUCCESS(200,"请求成功"),
    ERROR(500,"请求错误");

    private Integer code ;
    private String message ;

    ResponseCode(){};

    ResponseCode(Integer code ,String message){
        this.code = code ;
        this.message = message ;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static String getMessage(String name){
        for (ResponseCode item : values()){
            if(item.name().equals(name)){
                return item.message;
            }
        }
        return null ;
    }
}
