package com.check.base;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

// 统一结果返回类
@Data
@Component
@SuppressWarnings("all")
public class Result<T> implements Serializable {
    int count ; //数据数量
    Integer code ;   // 代码
    String msg ;       // 信息
    List<T> datas ; // 返回数据 ，返回多条数据
    Map<Object,Object> map;
    T data ;        // 任何类型的条件,返回一条数据

    public Result(){};

    public Result(int count) {
        this.count = count;
    }

    public Result(Integer code , String msg){
        super();
        this.code = code ;
        this.msg =msg ;
    }

    public Result(int count, Integer code, String msg, List<T> datas) {
        this.count = count;
        this.code = code;
        this.msg = msg;
        this.datas = datas;
        this.data = null ;
    }

    public Result(int count, Integer code, String msg, T data) {
        this.count = count;
        this.code = code;
        this.msg = msg;
        this.datas = null ;
        this.data = data;
    }

    public Result(int count, Integer code, String msg, List<T> datas, T data) {
        this.count = count;
        this.code = code;
        this.msg = msg;
        this.datas = datas;
        this.data = data;
    }

    public Result(T data){
        this.data = data ;
    }

    public static Result success() {
        Result result =new Result<>();
        result.setCode(ResponseCode.SUCCESS.getCode());
        result.setMsg(ResponseCode.SUCCESS.getMessage());
        return result;
    }

    public static Result success(Integer code ,String message){
        Result result = new Result<>();
        result.setCode(code);
        result.setMsg(message);
        return result ;
    }

    public static Result success(int count, Integer code, String msg, Object data){
        Result result = new Result<>();
        result.setCode(code);
        result.setMsg(msg);
        result.setCount(count);
        result.setData(data);
        return result ;
    }

    public static Result success(int count, Object data){
        Result result = new Result<>();
        result.setCode(200);
        result.setMsg("成功");
        result.setData(data);
        return result;
    }

    public static Result success(int count, List<?> datas) {
        Result result =new Result<>();
        result.setCount(count);
        result.setCode(ResponseCode.SUCCESS.getCode());
        result.setMsg(ResponseCode.SUCCESS.getMessage());
        result.setData(null);
        result.setDatas(datas);
        return result;
    }
//
//    public static Result success(int count, Object data) {
//        Result result =new Result<>();
//        result.setCount(count);
//        result.setCode(ResponseCode.SUCCESS.getCode());
//        result.setMsg(ResponseCode.SUCCESS.getMessage());
//        result.setData(data);
//        result.setDatas(null);
//        return result;
//    }

    public static Result success(int count, Integer code, String msg, List<?> datas){
        Result result = new Result<>();
        result.setCode(code);
        result.setMsg(msg);
        result.setCount(count);
        result.setDatas(datas);
        return result ;
    }

    public static Result failure(){
        Result result = new Result<>();
        result.setCode(ResponseCode.FAIL.getCode());
        result.setMsg(ResponseCode.FAIL.getMessage());
        return result ;
    }

    public static Result failure(Integer code , String message){
        Result result = new Result<>();
        result.setCode(code);
        result.setMsg(message);
        return result ;
    }

    public static <T> Result<T> success(T data) {
        Result result =new Result<>(data);
        result.setCode(ResponseCode.SUCCESS.getCode());
        result.setMsg(ResponseCode.SUCCESS.getMessage());
        return result;
    }

    public static <T> Result<T> success(int count) {
        Result result =new Result<>(count);
        result.setCode(ResponseCode.SUCCESS.getCode());
        result.setMsg(ResponseCode.SUCCESS.getMessage());
        return result;
    }

    public static <T> Result<T> error(){
        Result result = new Result<>();
        result.setCode(ResponseCode.ERROR.getCode());
        result.setMsg(ResponseCode.ERROR.getMessage());
        return result;
    }

    public static <T> Result<T> error(Integer code,String msg) {
        Result result = new Result<>();
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    public static<T> Result<T> success(Integer code,String msg,Map map){
        Result result = new Result<>();
        result.setCode(code);
        result.setMsg(msg);
        result.setMap(map);
        return result;
    }

}
