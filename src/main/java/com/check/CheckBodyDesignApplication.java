package com.check;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CheckBodyDesignApplication {

    public static void main(String[] args) {
        SpringApplication.run(CheckBodyDesignApplication.class, args);
    }

}
