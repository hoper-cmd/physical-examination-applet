package com.check.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Data
@Component
@NoArgsConstructor
@AllArgsConstructor
public class MedicalRecord {

    private Integer recordId;
    private String reservationNumber;
    private String doctorNumber;
    private String patientNumber;
    private String treatedStartTime;
    private String treatedEndedTime;
}
