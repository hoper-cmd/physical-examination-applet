package com.check.pojo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Data
@Component
@NoArgsConstructor
@AllArgsConstructor
public class DoctorDTO {
    private String realName;
    private String phoneNumber;
    private String idcardNumber;
    private String sex ;
    private String birth;
    private String livingPlace;
    private String nation ;
    private Integer doctorId;
    private String doctorNumber;
    private String doctorName;
    private String doctorTitle;
    private String hospitalSupports;
    private String hospitalDepartment;
    private String doctorAccount;
}
