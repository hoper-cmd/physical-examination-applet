package com.check.pojo.dto;

import com.check.pojo.TransObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@Component
@NoArgsConstructor
@AllArgsConstructor
public class HealthFamilyTransDTO {
    private String openid ;
    private Integer isEarly ;
    private List<TransObject> transObj ;
}
