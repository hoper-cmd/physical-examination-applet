package com.check.pojo.dto;

import com.check.pojo.SelectObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@Component
@NoArgsConstructor
@AllArgsConstructor
public class SelectObjectDTO {
    private String openid ;
    private List<SelectObject> tranObj ;

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public List<SelectObject> getTranObj() {
        return tranObj;
    }

    public void setTranObj(List<SelectObject> tranObj) {
        this.tranObj = tranObj;
    }
}
