package com.check.pojo.dto;

import com.check.pojo.StringObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@Component
@AllArgsConstructor
@NoArgsConstructor
public class StringObjectDTO {
    private String openid ;
    private List<StringObject> transObj;
}
