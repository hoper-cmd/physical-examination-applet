package com.check.pojo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Data
@Component
@NoArgsConstructor
@AllArgsConstructor
public class CommandDTO {
    private String commandAccount;
    private String openid ;
    private String realName;
    private String phoneNumber;
    private String idcardNumber;
    private String sex ;
    private String birth;
    private String livingPlace;
    private String nation ;
}
