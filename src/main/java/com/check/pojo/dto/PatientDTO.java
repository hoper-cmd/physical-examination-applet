package com.check.pojo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Data
@Component
@NoArgsConstructor
@AllArgsConstructor
public class PatientDTO {
    private String realName;
    private String phoneNumber;
    private String idcardNumber;
    private String sex ;
    private String birth;
    private String livingPlace;
    private String nation ;
    private String openid;
    private Integer patientId;
    private String patientNumber;
    private String patientName;
    private String patientJob;
    private String marryCondition;
    private String educationExtend;
}
