package com.check.pojo.dto;

import com.check.pojo.TransObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@Component
@NoArgsConstructor
@AllArgsConstructor
public class TransObjectDTO {
    private String openid ;
    private List<TransObject> transObj ;
}
