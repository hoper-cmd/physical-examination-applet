package com.check.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Data
@Component
@ApiModel("健康史_家族史")
@NoArgsConstructor
@AllArgsConstructor
public class HealthyFamily {

    @ApiModelProperty("表id")
    private Integer hfId;

    @ApiModelProperty("选项编号")
    private String option;

    @ApiModelProperty("选项内容")
    private String optionContent;

    @ApiModelProperty("体检项目")
    private String screeningProgramme ;

    @ApiModelProperty("最低年限")
    private Integer lowAge ;

    @ApiModelProperty("是否早发")
    private Integer isEarly ;

    @ApiModelProperty("题目编号")
    private String titleNum ;
}
