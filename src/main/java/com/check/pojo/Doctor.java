package com.check.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Data
@Component
@NoArgsConstructor
@AllArgsConstructor
public class Doctor {
    private Integer doctorId;
    private String doctorNumber;
    private String doctorOpenid;
    private String doctorName;
    private String doctorTitle;
    private String hospitalSupports;
    private String hospitalDepartment;
}
