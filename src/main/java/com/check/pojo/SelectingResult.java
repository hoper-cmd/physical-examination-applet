package com.check.pojo;

import lombok.ToString;

import java.io.Serializable;

/**
 * (SelectingResult)实体类
 *
 * @author makejava
 * @since 2022-05-05 00:34:13
 */
@ToString
public class SelectingResult implements Serializable {
    private static final long serialVersionUID = -17156412456826620L;
    
    private String screening;
    
    private String project;


    public String getScreening() {
        return screening;
    }

    public void setScreening(String screening) {
        this.screening = screening;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

}

