package com.check.pojo;

import java.io.Serializable;

/**
 * (PhysicalResult)实体类
 *
 * @author makejava
 * @since 2022-05-10 15:27:34
 */
public class PhysicalResult implements Serializable {
    private static final long serialVersionUID = 437544500095964826L;
    
    private Integer resultid;
    
    private Integer projectid;
    
    private Integer inspectionid;

    /**
     * 检验项目
     */
    private String inspection;
    /**
     * 单位
     */
    private String unit;
    /**
     * 参考范围
     */
    private String range;
    
    private String resultvalue;

    private String normal;

    public String getInspection() {
        return inspection;
    }

    public void setInspection(String inspection) {
        this.inspection = inspection;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }

    public Integer getResultid() {
        return resultid;
    }

    public void setResultid(Integer resultid) {
        this.resultid = resultid;
    }

    public Integer getProjectid() {
        return projectid;
    }

    public void setProjectid(Integer projectid) {
        this.projectid = projectid;
    }

    public Integer getInspectionid() {
        return inspectionid;
    }

    public void setInspectionid(Integer inspectionid) {
        this.inspectionid = inspectionid;
    }

    public String getResultvalue() {
        return resultvalue;
    }

    public void setResultvalue(String resultvalue) {
        this.resultvalue = resultvalue;
    }

    public String getNormal() {
        return normal;
    }

    public void setNormal(String normal) {
        this.normal = normal;
    }
}

