package com.check.pojo;

import java.io.Serializable;

/**
 * (InspectionItem)实体类
 *
 * @author makejava
 * @since 2022-05-10 15:27:34
 */
public class InspectionItem implements Serializable {
    private static final long serialVersionUID = 162618687044325348L;
    /**
     * 检验项目id
     */
    private Integer inspectionid;
    /**
     * 检验项目
     */
    private String inspection;
    /**
     * 单位
     */
    private String unit;
    /**
     * 参考范围
     */
    private String range;
    /**
     * 项目id
     */
    private Integer projectid;


    public Integer getInspectionid() {
        return inspectionid;
    }

    public void setInspectionid(Integer inspectionid) {
        this.inspectionid = inspectionid;
    }

    public String getInspection() {
        return inspection;
    }

    public void setInspection(String inspection) {
        this.inspection = inspection;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }

    public Integer getProjectid() {
        return projectid;
    }

    public void setProjectid(Integer projectid) {
        this.projectid = projectid;
    }

}

