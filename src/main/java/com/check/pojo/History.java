package com.check.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Data
@Component
@ApiModel("家族史历史记录")
@NoArgsConstructor
@AllArgsConstructor
public class History {
    @ApiModelProperty("表id")
    private Integer hisId;

    @ApiModelProperty("唯一标识")
    private String openid ;

    @ApiModelProperty("查询结果")
    private String result ;

    @ApiModelProperty("填写时间")
    private String fillinTime ;

    @ApiModelProperty("组别码")
    private Integer ids ;
}
