package com.check.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Data
@Component
@NoArgsConstructor
@AllArgsConstructor
public class Reservation {
    private String reservationNumber;
    private String patientNumber;
    private String doctorNumber;
    private String treatedCampus;
    private String treatedDepartment;
    private String reservationEndedTime;
    private String reservationStartTime;
    private String reservationCreatedTime;
    private Integer isCanceled;
    private Integer isEnded;
    private Integer isDelete;
}
