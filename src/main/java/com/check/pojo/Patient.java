package com.check.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Data
@Component
@NoArgsConstructor
@AllArgsConstructor
public class Patient {
    private Integer patientId;
    private String patientNumber;
    private String patientName;
    private String patientOpenid;
    private String patientJob;
    private String marryCondition;
    private String educationExtend;
}
