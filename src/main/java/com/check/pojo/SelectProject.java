package com.check.pojo;

import java.util.List;

/**
 * (SelectProject)实体类
 *
 * @author makejava
 * @since 2022-05-09 18:26:51
 */
public class SelectProject{
    /**
     * 项目id
     */
    private Integer projectid;
    
    private String projectname;
    /**
     * openid
     */
    private String openid;
    /**
     * 支付时间
     */
    private String paytime;

    private List<PhysicalResult> physicalResults;

    public List<PhysicalResult> getPhysicalResults() {
        return physicalResults;
    }

    public void setPhysicalResults(List<PhysicalResult> physicalResults) {
        this.physicalResults = physicalResults;
    }

    public SelectProject(String projectname, String openid, String paytime) {
    }

    public Integer getProjectid() {
        return projectid;
    }

    public void setProjectid(Integer projectid) {
        this.projectid = projectid;
    }

    public String getProjectname() {
        return projectname;
    }

    public void setProjectname(String projectname) {
        this.projectname = projectname;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getPaytime() {
        return paytime;
    }

    public void setPaytime(String paytime) {
        this.paytime = paytime;
    }
}

