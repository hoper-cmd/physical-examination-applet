package com.check.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Data
@Component
@NoArgsConstructor
@AllArgsConstructor
public class BookSchedule {
    private int bookId;
    private String doctorOpenid;
    private String bookDate;
    private String bookTime;
    private int maxBookMember;
}
