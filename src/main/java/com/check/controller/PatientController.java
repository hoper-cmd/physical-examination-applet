package com.check.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.check.base.Result;
import com.check.pojo.ResultProject;
import com.check.pojo.dto.PatientDTO;
import com.check.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/patient")
public class PatientController {

    @Autowired
    private PatientService patientService;

    @RequestMapping("/showPatientInfo")
    @ResponseBody
    public Result showPatientInfo(String patientOpenid){
        return patientService.showPatientInfo(patientOpenid);
    }

    /**
     * 获取当前有号的医生
     * @param currentTab 0-当日有号   1-全部有号信息
     * @return 医生姓名，医生职称，所属院区、所属科室、有号的日期，有号的时间,医生手机号码
     */
    @RequestMapping("/getAllBookDoctors")
    @ResponseBody
    public List<Map<String, Object>> getAllBookDoctors(Integer currentTab){
        return patientService.getAllBookDoctors(currentTab);
    }

    @PostMapping("/insertReservation")
    @ResponseBody
    public Result insertReservation(String patientOpenid, String doctorPhoneNumber, String bookDateAndTime){
        return patientService.insertReservation(patientOpenid,doctorPhoneNumber,bookDateAndTime);
    }

    @RequestMapping("/getRecords")
    @ResponseBody
    public List<Map<String,Object>> getRecords(String patientOpenid){
       return patientService.getRecordsByPatientOpenid(patientOpenid);
    }

    @RequestMapping("/endThisReservation")
    @ResponseBody
    public void endThisReservation(String reservationNumber){
        patientService.endThisReservation(reservationNumber);
    }

    @PostMapping("/editPatient")
    @ResponseBody
    public Result editPatient(@RequestBody JSONObject jsonObject){
        // 解析JSON，转成对象
        String string = JSON.parseObject(String.valueOf(jsonObject), Map.class).get("jsonObject").toString();
        PatientDTO patientDTO = JSON.parseObject(string, PatientDTO.class);
        return patientService.editPatient(patientDTO);
    }

    @RequestMapping("/allOptions")
    @ResponseBody
    public List<ResultProject> allOptions() {
        return patientService.selectAllResult();
    }

    @PostMapping("/multiple")
    @ResponseBody
    public List<String> getProjectsBuResults(@RequestParam(value = "Option_list[]", required = false) String[] Option_list) {
        System.out.println("用户选择的检查结果（需找到对应的项目）"+ Arrays.toString(Option_list));
        return patientService.queryProjectByOptions(Option_list);
    }

    @PostMapping("/price")
    @ResponseBody
    public Map<String, List> getPrice(@RequestParam(value = "checkProject[]" ,required=false)String[] checkProject){
        System.out.println("项目名称（需获取价格）"+ Arrays.toString(checkProject));
        return patientService.queryByppProject(checkProject);
    }
}
