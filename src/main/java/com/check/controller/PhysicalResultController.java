package com.check.controller;


import com.alibaba.fastjson.JSONObject;
import com.check.pojo.InspectionItem;
import com.check.pojo.PhysicalResult;
import com.check.pojo.SelectProject;
import com.check.service.InspectionItemService;
import com.check.service.PhysicalResultService;
import com.check.service.SelectProjectService;
import com.check.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * (PhysicalResult)表控制层
 *
 * @author makejava
 * @since 2022-05-10 15:27:34
 */
@RestController
@RequestMapping("physicalResult")
public class PhysicalResultController {
    /**
     * 服务对象
     */
    @Autowired
    private PhysicalResultService physicalResultService;

    @Autowired
    private SelectProjectService selectProjectService;

    @Autowired
    private InspectionItemService inspectionItemService;

    @Autowired
    private UserService userService;

    @PostMapping("/paytime")
    @ResponseBody
    public String getPayTimes(@RequestParam("openid") String openid){
        List<String> payTimes = selectProjectService.getPayTime(openid);
        Set<String> set = new HashSet<>();
        set.addAll(payTimes);

        JSONObject result = new JSONObject();
        if(set != null) {
            System.out.println(set);
            result.put("status", "success");
            result.put("paytimes", set);
        }
        else {
            result.put("status", "fail");
        }
        return result.toJSONString();
    }

    @PostMapping("/selecttime")
    @ResponseBody
    public String getProjectsId(@RequestParam("openid") String openid, @RequestParam("paytime") String paytime) {
        JSONObject result = new JSONObject();
        // 获取所有体检项目
        List<SelectProject> inspectionItems = selectProjectService.selectInspectionItemByProject(openid, paytime);
        if (inspectionItems.size() != 0){
            result.put("status", "success");
            result.put("projectsResult", inspectionItems);
            return result.toJSONString();
        }else{
            List<SelectProject> projectsResult = selectProjectService.getProjectEntity(openid, paytime);
            // 获取所有检验项目
            List<InspectionItem> inspectionItemList = inspectionItemService.selectAllInspectionItem();
            Random inspectionItemRandom = new Random();
            int normalFlag = 5;
            for(int j = 0;j < projectsResult.size();j++) {
                List<PhysicalResult> physicalResults = new ArrayList<>();
                for (int i = 0; i < 3; i++) {
                    PhysicalResult physicalResult = new PhysicalResult();
                    physicalResult.setProjectid(projectsResult.get(j).getProjectid());
                    int randNormal = inspectionItemRandom.nextInt(10);
                    String normal = randNormal > normalFlag?"正常":"异常";
                    physicalResult.setNormal(normal);
                    //随机数用于获得一个体检项目的多条检验项目
                    int inspection = inspectionItemRandom.nextInt(inspectionItemList.size());
                    InspectionItem inspectionItem = inspectionItemList.get(inspection);
                    physicalResult.setInspectionid(inspectionItem.getInspectionid());
                    physicalResult.setInspection(inspectionItem.getInspection());
                    physicalResult.setRange(inspectionItem.getRange());
                    physicalResult.setUnit(inspectionItem.getUnit());
                    String[] range = inspectionItem.getRange().replace(" ", "").split("~");
                    double resValue = 0.0;
                    if ("正常".equals(normal)){
                        resValue = inspectionItemRandom.nextDouble() * Double.parseDouble(range[1]) + Double.parseDouble(range[0]);
                    }else {
                        resValue = inspectionItemRandom.nextDouble() * Double.parseDouble(range[1]) + Double.parseDouble(range[1]);
                    }
                    physicalResult.setResultvalue(String.valueOf(resValue));
                    physicalResultService.insertResort(physicalResult);
                    physicalResults.add(physicalResult);
                }
                projectsResult.get(j).setPhysicalResults(physicalResults);
                if(projectsResult != null) {
                    result.put("status", "success");
                    result.put("projectsResult", projectsResult);
                }
                else {
                    result.put("status", "fail");
                }
            }
            return result.toJSONString();
        }
    }
}

