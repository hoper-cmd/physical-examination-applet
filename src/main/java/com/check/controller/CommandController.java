package com.check.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.check.base.Result;
import com.check.pojo.Reservation;
import com.check.pojo.dto.CommandDTO;
import com.check.pojo.dto.DoctorDTO;
import com.check.pojo.dto.PatientDTO;
import com.check.service.CommandService;
import com.check.service.DoctorService;
import com.check.service.PatientService;
import com.check.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/command")
public class CommandController {

    @Autowired
    private DoctorService doctorService;

    @Autowired
    private PatientService patientService;

    @Autowired
    private UserService userService;

    @Autowired
    private CommandService commandService;

    @RequestMapping("/getCommandAccount")
    @ResponseBody
    public List<String> getCommandAccount(){
        return commandService.getCommandAccount();
    }

    @RequestMapping("/showCommandInfo")
    @ResponseBody
    public Result showCommandInfo(String commandAccount){
        return commandService.showCommandInfo(commandAccount);
    }

    @RequestMapping("/getDoctorsInfo")
    @ResponseBody
    public List<Map<String,Object>> getDoctorsInfo(){
        return doctorService.getAllDoctorsInfo();
    }

    @PostMapping("/editDoctor")
    @ResponseBody
    public Result editDoctor(@RequestBody JSONObject jsonObject){
        // 解析JSON，转成对象
        String string = JSON.parseObject(String.valueOf(jsonObject), Map.class).get("jsonObject").toString();
        DoctorDTO doctorDTO = JSON.parseObject(string, DoctorDTO.class);

        return doctorService.editDoctor(doctorDTO);
    }

    @RequestMapping("/getPatientsInfo")
    @ResponseBody
    public List<Map<String,Object>> getPatientsInfo(){
        return patientService.getAllPatientsInfo();
    }

    @PostMapping("/editPatient")
    @ResponseBody
    public void editPatient(@RequestBody JSONObject jsonObject){
        // 解析JSON，转成对象
        String string = JSON.parseObject(String.valueOf(jsonObject), Map.class).get("jsonObject").toString();
        PatientDTO patientDTO = JSON.parseObject(string, PatientDTO.class);

        patientService.editPatient(patientDTO);
    }

    @RequestMapping("/getAllReservations")
    @ResponseBody
    public List<Map<String,Object>> getAllReservations(){
        return commandService.getAllReservations();
    }

    @RequestMapping("/endThisReservation")
    @ResponseBody
    public void endThisReservation(String reservationNumber){
        commandService.endThisReservation(reservationNumber);
    }

    @PostMapping("/editThisReservation")
    @ResponseBody
    public Result editThisReservation(@RequestBody JSONObject jsonObject){
        // 解析JSON，转成对象
        String string = JSON.parseObject(String.valueOf(jsonObject), Map.class).get("jsonObject").toString();
        Reservation reservation = JSON.parseObject(string, Reservation.class);
        String reservationEndedTime = reservation.getReservationEndedTime();

        if("".equals(reservationEndedTime) || null == reservationEndedTime){
            int result = commandService.editThisReservation(reservation);
            if(result > 0){
                return Result.success(200,"修改成功");
            }else {
                return Result.success(0,"修改失败，请联系运维人员");
            }
        }else {
            return Result.error(500,"该预约已结束,不可修改");
        }
    }

    @PostMapping("/editPersonalInfo")
    @ResponseBody
    public Result editPersonalInfo(@RequestBody CommandDTO commandDTO){
        return commandService.editPersonalInfo(commandDTO);
    }
}
