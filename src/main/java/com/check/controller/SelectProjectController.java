package com.check.controller;

//选择体检项目并支付的结果

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.check.pojo.SelectObject;
import com.check.pojo.SelectProject;
import com.check.pojo.StringObject;
import com.check.pojo.TransObject;
import com.check.pojo.dto.HealthFamilyTransDTO;
import com.check.pojo.dto.StringObjectDTO;
import com.check.pojo.dto.TransObjectDTO;
import com.check.service.SelectProjectService;
import com.check.util.JSONUtil;
import org.apache.ibatis.annotations.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * (SelectProject)表控制层
 *
 * @author makejava
 * @since 2022-05-09 18:26:50
 */
@RestController
@RequestMapping("selectProject")
public class SelectProjectController {
    @Autowired
    private SelectProjectService selectProjectService;

    @PostMapping("/select")
    @ResponseBody
    public boolean insertProjects(@RequestBody JSONObject transObject) {
        // 解析JSON，转成对象
        String string = JSON.parseObject(String.valueOf(transObject), Map.class).get("transObject").toString();
        StringObjectDTO transObjectDTO = JSON.parseObject(string, StringObjectDTO.class);

        // 获取所需要的值
        String openid = transObjectDTO.getOpenid();
        List<StringObject> list = transObjectDTO.getTransObj();

        // 确定时间
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String payTime = sdf.format(date);
        System.out.println("payTime:" + payTime);

        for (int i = 0; i < list.size(); i++) {
            String projectName = list.get(i).getValue();
            selectProjectService.insertProjects(projectName,openid,payTime);
        }
        return true;
    }
}

