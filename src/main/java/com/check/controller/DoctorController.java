package com.check.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.check.base.Result;
import com.check.pojo.dto.DoctorDTO;
import com.check.service.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/doctor")
public class DoctorController {

    @Autowired
    private DoctorService doctorService;

    @RequestMapping("/getAccounts")
    @ResponseBody
    public List<String> getDoctorAccounts(){
        return doctorService.getDoctorAccounts();
    }

    @RequestMapping("/showDoctorInfo")
    @ResponseBody
    public Result showDoctorInfo(String doctorAccount){
        return doctorService.showDoctorInfo(doctorAccount);
    }

    @RequestMapping("/getDoctorInfo")
    @ResponseBody
    public Map<String,Object> getDoctorInfo(String openid){
        return doctorService.getDoctorInfo(openid);
    }

    @RequestMapping("/getBookScheduleDateList")
    @ResponseBody
    public List<String> getBookScheduleDateList(String doctorAccount){
        return doctorService.getBookScheduleDateList(doctorAccount);
    }

    @RequestMapping("/getBookScheduleTimeList")
    @ResponseBody
    public List<String> getBookScheduleTimeList(String doctorAccount,String bookDate){
        return doctorService.getBookScheduleTime(doctorAccount,bookDate);
    }

    @RequestMapping("/insertBookSchedule")
    @ResponseBody
    public void insertBookSchedule(String openid,String bookDate,String time, Integer maxBookMember){
        doctorService.insertBookSchedule(openid,bookDate,time,maxBookMember);
    }

    @RequestMapping("/insertBookScheduleDate")
    @ResponseBody
    public void insertBookScheduleDate(String doctorAccount,String[] bookDateList){
        doctorService.insertBookScheduleDate(doctorAccount,bookDateList);
    }

    @RequestMapping("/insertBookScheduleTime")
    @ResponseBody
    public void insertBookScheduleTime(String doctorAccount,String bookDate, String[] timeList){
        doctorService.insertBookScheduleTime(doctorAccount,bookDate,timeList);
    }

    @RequestMapping("/getReservation")
    @ResponseBody
    public List<Map<String,Object>> getReservationByDoctorOpenid(String doctorOpenid){
        return doctorService.getReservationByDoctorOpenid(doctorOpenid);
    }

    @RequestMapping("/getRecords")
    @ResponseBody
    public List<Map<String,Object>> getRecords(String doctorAccount){
        return doctorService.getRecordsByDoctorAccount(doctorAccount);
    }

    @PostMapping("/editDoctor")
    @ResponseBody
    public void editDoctor(@RequestBody JSONObject jsonObject){
        // 解析JSON，转成对象
        String string = JSON.parseObject(String.valueOf(jsonObject), Map.class).get("jsonObject").toString();
        DoctorDTO doctorDTO = JSON.parseObject(string, DoctorDTO.class);
        doctorService.editDoctor(doctorDTO);
    }

    @RequestMapping("/deleteReservation")
    @ResponseBody
    public void deleteReservation(String reservationNumber){
        doctorService.deleteReservation(reservationNumber);
    }

    @RequestMapping("/endThisReservation")
    @ResponseBody
    public void endThisReservation(String reservationNumber){
        doctorService.endThisReservation(reservationNumber);
    }

    @PostMapping("/editPersonalInfo")
    @ResponseBody
    public Result editPersonalInfo(@RequestBody JSONObject jsonObject){
        // 解析JSON，转成对象
        String string = JSON.parseObject(String.valueOf(jsonObject), Map.class).get("jsonObject").toString();
        DoctorDTO doctorDTO = JSON.parseObject(string, DoctorDTO.class);
        return doctorService.editDoctor(doctorDTO);
    }
}
