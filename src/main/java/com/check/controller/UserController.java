package com.check.controller;

import com.check.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping("/wxLogin")
    @ResponseBody
    public Map<String,Object> login(@RequestParam(value = "code",required = false) String code) {
        return userService.wxLogin(code);
    }
}