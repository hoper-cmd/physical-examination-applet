package com.check.controller;

//调查问卷推荐后，通过筛查展示体检项目的结果

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.check.pojo.History;
import com.check.pojo.ProjectPrice;
import com.check.pojo.SelectingResult;
import com.check.service.HistoryService;
import com.check.service.ProjectPriceService;
import com.check.service.SelectingResultService;
import com.check.util.HistoryUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * (SelectingResult)表控制层
 *
 * @author makejava
 * @since 2022-05-03 19:33:00
 */

@Controller
@RequestMapping("/selectingResult")
public class SelectingResultController {

    @Autowired
    private SelectingResultService selectingResultService;

    @Autowired
    private HistoryService historyService;

    @Autowired
    private ProjectPriceService projectPriceService;

    @PostMapping("/project")
    @ResponseBody
    public String getProjects(@RequestParam("openid") String openid) {
        List<String> fillinTimes = historyService.getTimeList(openid);
        Set<String> set = new HashSet<>();
        set.addAll(fillinTimes);
        List<List<SelectingResult>> recommendProject = new ArrayList<>();
        BigDecimal dateTime = BigDecimal.valueOf(0);
        String fillin = set.iterator().next();
        for(String fillinTime : set){
            BigDecimal fillinDate = HistoryUtil.dateToStamp(fillinTime);
            if (dateTime.compareTo(fillinDate) == -1){
                dateTime = fillinDate;
                fillin = fillinTime;
            }
        }
        List<History> histories = historyService.getHistoryByOpenidAndFillInTime(openid, fillin);
        for (int i = 0; i < histories.size(); i++) {
            recommendProject.add(selectingResultService.getRecommendProject(histories.get(i).getResult()));
        }

        Set<String> projectList = new HashSet<>();

        for (List<SelectingResult> selectingResults : recommendProject) {
            for (SelectingResult selectingResult : selectingResults) {
                String[] projectInfos = selectingResult.getProject().split(",");
                for (String projectInfo : projectInfos) {
                    projectList.add(projectInfo);
                }
            }
        }

        System.out.println(projectList);

        List<ProjectPrice> projectPrices = new ArrayList<>();
        List<String> priceList = new ArrayList<>();
        String str = "{\"pp_id\":\"1\",\"pp_project\":\"一般检查\",\"pp_price\":\"0\"}";;
        ProjectPrice price = JSON.parseObject(str,ProjectPrice.class);

        for (String project : projectList){
            if (projectPriceService.selectByppProject(project) == null){
                projectPrices.add(price);
            }else {
                projectPrices.add(projectPriceService.selectByppProject(project));
            }
        }
        for (ProjectPrice projectPrice : projectPrices){
            String[] priceInfos = projectPrice.getPpPrice().toString().split(",");
            for (String priceInfo : priceInfos) {
                priceList.add(priceInfo);
            }
        }


        JSONObject result = new JSONObject();
        if(projectList != null) {
            System.out.println(recommendProject.toString());
            result.put("status", "success");
            result.put("projectList", projectList);
            result.put("priceList" , priceList);
        }
        else {
            result.put("status", "fail");
        }
        return result.toJSONString();
    }

}

