package com.check.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.check.base.Result;
import com.check.mapper.HabitWineMapper;
import com.check.pojo.TransObject;
import com.check.pojo.dto.HealthFamilyTransDTO;
import com.check.service.*;
import com.check.util.JSONUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Controller
@RequestMapping("/questionnaire")
@SuppressWarnings("all")
public class QuestionnaireController {

    @Autowired
    private HealthyFamilyService healthyFamilyService ;

    @Autowired
    private HealthPresentService healthPresentService;

    @Autowired
    private HealthMenstrualService healthMenstrualService;

    @Autowired
    private BodySymptomsService bodySymptomsService;

    @Autowired
    private HabitSmokeService habitSmokeService;

    @Autowired
    private HabitDietService habitDietService;

    @Autowired
    private HabitWineService habitWineService;

    @Autowired
    private MentalStressService mentalStressService;

    @PostMapping("/family")
    @ResponseBody
    public Result<String> recommendFamilySP(@RequestBody JSONObject transObject) throws JSONException {

        // 解析JSON，转成对象
        String string = JSON.parseObject(String.valueOf(transObject), Map.class).get("familyObject").toString();
        HealthFamilyTransDTO family = JSON.parseObject(string, HealthFamilyTransDTO.class);

        // 获取所需要的值
        String openid = family.getOpenid();
        Integer isEarly = family.getIsEarly();
        List<TransObject> transObj = family.getTransObj();

        // 查询数据并返回
        Set<String> set = healthyFamilyService.recommendScreeningProgramme(transObj, isEarly, openid);
        return Result.success(set.size(), set);
    }


    @PostMapping("/present")
    @ResponseBody
    public Result<String> recommendPresentSP(@RequestBody JSONObject transObject) throws JSONException {
        Map map = JSONUtil.JSON2Map(transObject);
        // 查询数据并返回
        Set<String> set = healthPresentService.recommendScreeningProgramme(map);
        return Result.success(set.size(), set);
    }

    @PostMapping("/menstrual")
    @ResponseBody
    public Result<String> recommendMenstrualSP(@RequestBody JSONObject transObject) throws JSONException {
        Map map = JSONUtil.JSON2Map(transObject);
        
        // 查询数据并返回
        Set<String> set = healthMenstrualService.recommendScreeningProgramme(map);
        return Result.success(set.size(), set);
    }

    @PostMapping("/symptoms")
    @ResponseBody
    public Result<String> recommendSymptomsSP(@RequestBody JSONObject transObject) throws JSONException {
        Map map = JSONUtil.JSON2Map(transObject);

        // 查询数据并返回
        Set<String> set = bodySymptomsService.recommendScreeningProgramme(map);
        return Result.success(set.size(), set);
    }

    @PostMapping("/smoke")
    @ResponseBody
    public Result<String> recommendSmokeSP(@RequestBody JSONObject transObject) throws JSONException {
        Map map = JSONUtil.JSON2Map(transObject);

        // 查询数据并返回
        Set<String> set = habitSmokeService.recommendScreeningProgramme(map);
        return Result.success(set.size(), set);
    }

    @PostMapping("/diet")
    @ResponseBody
    public Result<String> recommendDietSP(@RequestBody JSONObject transObject) throws JSONException {
        Map map = JSONUtil.JSON2Map(transObject);

        // 查询数据并返回
        Set<String> set = habitDietService.recommendScreeningProgramme(map);
        return Result.success(set.size(), set);
    }

    @PostMapping("/wine")
    @ResponseBody
    public Result<String> recommendWineSP(@RequestBody JSONObject transObject) throws JSONException {
        Map map = JSONUtil.JSON2Map(transObject);

        // 查询数据并返回
        Set<String> set = habitWineService.recommendScreeningProgramme(map);
        return Result.success(set.size(), set);
    }

    @PostMapping("/stress")
    @ResponseBody
    public Result<String> recommendStressSP(@RequestBody JSONObject transObject){
        Map map = JSONUtil.JSON2Map(transObject);

        // 查询数据并返回
        Set<String> set = mentalStressService.recommendScreeningProgramme(map);
        return Result.success(set.size(), set);
    }
}
