package com.check.mapper;

import com.check.pojo.HealthPresent;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface HealthPresentMapper {
    /**
     * 根据选项查询记录
     * @param titleNum 题目编号
     * @return 符合条件的选项集
     */
    List<HealthPresent> getHealthPresentByTitleNum(@Param("titleNum") String titleNum);

    /**
     * 根据标题和选项确定唯一元素
     * @param titleNum 题目编号
     * @param option 选项内容
     * @return 符合条件的一条记录
     */
    HealthPresent getHealthPresentByTitleNumAndOption(@Param("titleNum") String titleNum,
                                                      @Param("option") String option);

    /**
     * 查询推荐的体检项目
     * @param option 选项
     * @param lowAge 最低年龄
     * @return 体检项目集
     */
    List<String> getScreeningProgramme(@Param("titleNum") String titleNum,
                                       @Param("option") String option,
                                       @Param("lowAge") Integer lowAge);

    /**
     * 查询记录
     * @param HPId 表主键
     * @return 符合条件的一条记录
     */
    HealthPresent getHealthPresentByHFId(@Param("HPId") Integer HPId);
}
