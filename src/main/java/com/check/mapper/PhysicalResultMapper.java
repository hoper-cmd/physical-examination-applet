package com.check.mapper;

import com.check.pojo.PhysicalResult;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * (PhysicalResult)表数据库访问层
 *
 * @author makejava
 * @since 2022-05-10 15:27:34
 */
@Mapper
@Repository
public interface PhysicalResultMapper {
    int insertReport(@Param("physicalResult") PhysicalResult physicalResult);

    List<Integer> getInspectionIds(@Param("projectid") Integer projectid);

    List<String> selectNormal(@Param("projectid") Integer projectid, @Param("inspectionid") Integer inspectionid);
}

