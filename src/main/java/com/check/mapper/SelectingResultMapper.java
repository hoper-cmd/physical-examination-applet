package com.check.mapper;

import com.check.pojo.SelectingResult;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface SelectingResultMapper {

    /**
     * 根据筛查查询体检项目
     */
    List<SelectingResult> getRecommendProject(String screening);
}
