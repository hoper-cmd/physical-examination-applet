package com.check.mapper;

import com.check.pojo.UserRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserRoleMapper {

    void insertUserRole(UserRole userRole);

    /**
     * 获取UserROle的信息
     * @return com.check.pojo.UserRole
     */
    UserRole getUserRoleInfo(@Param("doctorOpenid") String doctorOpenid, @Param("roleId") Integer roleId);

    /**
     * 获取对应的账号
     * @param roleId 账号级别 1-管理员 2-医生 3-患者
     * @return list
     */
    List<String> getAccounts(Integer roleId);

    /**
     * 根据账号获取UserRole信息
     * @param account 医生账户
     * @return com.check.pojo.UserRole
     */
    UserRole getInfoByAccount(String account);
}
