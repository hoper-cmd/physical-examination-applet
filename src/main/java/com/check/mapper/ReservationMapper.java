package com.check.mapper;

import com.check.pojo.Reservation;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ReservationMapper {

    /**
     * 获取预约单信息
     * @param doctorNumber 医生编号
     * @return 预约单信息
     */
    List<Reservation> getReservationsByDoctorOpenid(String doctorNumber);

    /**
     * 假删除预约单
     * @param reservationNumber 预约单号
     */
    void deleteReservation(@Param("reservationNumber") String reservationNumber, @Param("reservationEndedTime") String reservationEndedTime);

    /**
     * 新增预约单
     * @param reservation 预约单
     * @return >0 成功  其余失败
     */
    int insertReservation(Reservation reservation);

    /**
     * 获取预约单信息
     * @param reservationNumber 预约单编号
     * @return com.check.pojo.Reservation
     */
    Reservation getReservation(String reservationNumber);

    /**
     * 获取预约单信息
     * @return com.check.pojo.Reservation
     */
    List<Reservation> getAllReservation();

    /**
     * 修改预约单信息
     * @param reservation 预约单信息
     * @return  >0成功  其余失败
     */
    int updateReservation(Reservation reservation);

    /**
     * 获取预约单信息
     * @param patientNumber 患者编号
     * @param doctorNumber 医生编号
     * @param bookDateAndTime 预约时间
     * @return 预约单信息
     */
    Reservation queryReservation(@Param("patientNumber") String patientNumber,
                                 @Param("doctorNumber") String doctorNumber,
                                 @Param("bookDateAndTime") String bookDateAndTime);
}
