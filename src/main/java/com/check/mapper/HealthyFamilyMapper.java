package com.check.mapper;

import com.check.pojo.HealthyFamily;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface HealthyFamilyMapper {
    /**
     * 根据选项查询记录
     * @param titleNum 题目编号
     * @return 符合条件的选项集
     */
    List<HealthyFamily> getHealthyFamilyByTitleNum(@Param("titleNum") String titleNum);

    /**
     * 根据标题和选项确定唯一元素
     * @param titleNum 题目编号
     * @param option 选项内容
     * @return 符合条件的一条记录
     */
    HealthyFamily getHealthyFamilyByTitleNumAndOption(@Param("titleNum") String titleNum,
                                                            @Param("option") String option);

    /**
     * 查询推荐的体检项目
     * @param option 选项
     * @param lowAge 最低年龄
     * @param isEarly 是否早发
     * @return 体检项目集
     */
    List<String> getScreeningProgramme(@Param("titleNum") String titleNum,
                                       @Param("option") String option,
                                       @Param("lowAge") Integer lowAge,
                                       @Param("isEarly") Integer isEarly);

    /**
     * 查询记录
     * @param HFId 表主键
     * @return 符合条件的一条记录
     */
    HealthyFamily getHealthyFamilyByHFId(@Param("HFId") Integer HFId);


}
