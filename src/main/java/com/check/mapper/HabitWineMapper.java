package com.check.mapper;

import com.check.pojo.HabitWine;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface HabitWineMapper {
    /**
     * 根据选项查询记录
     * @param titleNum 题目编号
     * @return 符合条件的选项集
     */
    List<HabitWine> getHabitWineByTitleNum(@Param("titleNum") String titleNum);

    /**
     * 根据标题和选项确定唯一元素
     * @param titleNum 题目编号
     * @param option 选项内容
     * @return 符合条件的一条记录
     */
    HabitWine getHabitWineByTitleNumAndOption(@Param("titleNum") String titleNum,
                                              @Param("option") String option);

    /**
     * 查询推荐的体检项目
     * @param option 选项
     * @param lowAge 最低年龄
     * @return 体检项目集
     */
    List<String> getScreeningProgramme(@Param("titleNum") String titleNum,
                                       @Param("option") String option,
                                       @Param("lowAge") Integer lowAge);

    /**
     * 查询记录
     * @param HWId 表主键
     * @return 符合条件的一条记录
     */
    HabitWine getHabitWineByHWId(@Param("HWId") Integer HWId);
}
