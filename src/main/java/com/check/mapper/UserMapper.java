package com.check.mapper;

import com.check.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface UserMapper {

    /**
     * 获取用户信息
     * @param openid 唯一标识
     * @return com.check.pojo.User
     */
    User getUserByOpenid(@Param("openid") String openid);

    /**
     * 插入用户信息
     * @param user 用户
     */
    void insertUser(User user);

    /**
     * 修改用户信息
     * @param user 用户
     */
    int updateUser(User user);

    /**
     * 获取用户信息
     * @param phoneNumber 手机号
     * @return com.check.pojo.User
     */
    User getUserByPhoneNumber(String phoneNumber);
}
