package com.check.mapper;

import com.check.pojo.Patient;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PatientMapper {

    /**
     * 获取患者的信息
     * @return com.check.pojo.Patient
     */
    Patient getPatientInfoByPatientNumber(String patientNumber);

    /**
     * 获取所有患者的信息
     * @return list
     */
    List<Patient> getAllPatientInfo();

    /**
     * 修改患者的信息
     * @param patient com.check.pojo.Patient
     * @return
     */
    int updatePatient(Patient patient);

    /**
     * 获取患者信息
     * @param patientOpenid 患者唯一标识
     * @return com.check.pojo.Patient
     */
    Patient getPatientInfo(String patientOpenid);

    /**
     * 插入患者信息
     * @param patient com.check.pojo.Patient
     * @return
     */
    int insertPatient(Patient patient);
}
