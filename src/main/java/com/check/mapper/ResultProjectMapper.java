package com.check.mapper;

import com.check.pojo.ResultProject;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ResultProjectMapper {

    /**
     * 返回所有的结果集
     * @return com.check.pojo.ResultProject
     */
    List<ResultProject> selectAllResult();

    /**
     * 查询一条记录
     * @param option 选项
     * @return com.check.pojo.ResultProject
     */
    ResultProject queryByResult(String option);

    /**
     * 通过ID查询单条数据
     *
     * @param rpId 主键
     * @return 实例对象
     */
    ResultProject queryById(Integer rpId);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<ResultProject> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param resultProject 实例对象
     * @return 对象列表
     */
    List<ResultProject> queryAll(ResultProject resultProject);

    /**
     * 新增数据
     *
     * @param resultProject 实例对象
     * @return 影响行数
     */
    int insert(ResultProject resultProject);

    /**
     * 修改数据
     *
     * @param resultProject 实例对象
     * @return 影响行数
     */
    int update(ResultProject resultProject);

    /**
     * 通过主键删除数据
     *
     * @param rpId 主键
     * @return 影响行数
     */
    int deleteById(Integer rpId);

    List<ResultProject> selectAll();

    List selectAllProject();
}
