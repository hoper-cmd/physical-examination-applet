package com.check.mapper;

import com.check.pojo.History;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface HistoryMapper {

    /**
     * 获取全部的History信息
     * @return List集合
     */
    List<History> getAllHistories();

    /**
     * 获取历史记录
     * @param openid 微信用户唯一标识
     * @return 符合条件的结果集
     */
    List<History> getHistoryById(@Param("openid") String openid);

    /**
     * @param openid 微信用户唯一标识
     * @param fillinTime 填写时间
     * @return 符合条件的结果集合
     */
    List<String> getResultsByOpenidAndFillinTime(@Param("openid") String openid,
                                              @Param("fillinTime") String fillinTime);

    /**
     * 获取历史记录集
     * @param openid 微信用户身份唯一标识
     * @param fillinTime 填写时间
     * @return 历史记录的集合
     */
    List<History> getHistoryByOpenidAndFillInTime(@Param("openid") String openid,
                                                  @Param("fillinTime") String fillinTime);

    /**
     * 同时间内，产生的历史记录
     * @param fillinTime 填写时间
     * @return 符合条件的结果集
     */
    List<History> getHistoryByTime(@Param("fillinTime") String fillinTime);

    /**
     *
     * @param openid 唯一标识
     * @param fillinTime 填写时间
     * @param ids 组别码
     * @return History
     */
    List<History> getHistory(@Param("openid") String openid,
                             @Param("fillinTime") String fillinTime,
                             @Param("ids") Integer ids);

    /**
     * 获取openid下的所有时间
     * @param openid 微信用户唯一标识
     * @return 所有的时间的集合
     */
    List<String> getTime(String openid);

    /**
     * 增加一条记录
     * @param history 对象
     * @return 整型数字（>0 成功； <=0 失败）
     */
    int insertHistory(History history);

    /**
     * 更新已经存在的历史记录的内容
     * @param history 对象
     * @return 整型数字（>0 成功； <=0 失败）
     */
    int updateHistory(History history);

    /**
     * 删除一组记录
     * @param openid 微信用户身份唯一标识
     * @return 整型数字（>0 成功； <=0 失败）
     */
    int deleteHistory(@Param("openid") String openid);

    /**
     * 删除特定记录
     * @param openid 微信用户身份唯一标识
     * @param fillinTime 填写时间
     * @return 整型数字（>0 成功； <=0 失败）
     */
    int deleteHistoryByOpenidAndFillInTime(@Param("openid") String openid,
                                             @Param("fillinTime") String fillinTime);


}
