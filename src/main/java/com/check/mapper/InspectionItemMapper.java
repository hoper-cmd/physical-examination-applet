package com.check.mapper;

import com.check.pojo.InspectionItem;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * (InspectionItem)表数据库访问层
 *
 * @author makejava
 * @since 2022-05-10 15:27:33
 */
@Mapper
@Repository
public interface InspectionItemMapper {

    List<InspectionItem> selectAllInspectionItem();

    List<InspectionItem> selectByInspectionId(@Param("inspectionid") Integer inspectionid);
}

