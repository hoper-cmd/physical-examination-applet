package com.check.mapper;

import com.check.pojo.MedicalRecord;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface MedicalRecordMapper {
    /**
     * 插入历史记录
     * @param medicalRecord 历史记录信息
     * @return >0 成功 其余失败
     */
    int insertMedicalRecord(MedicalRecord medicalRecord);

    /**
     * 获取就诊记录信息
     * @param patientNumber 患者编号
     * @return 就诊记录信息
     */
    List<MedicalRecord> getRecordsByPatientNumber(String patientNumber);

    /**
     * 根据预约单获取记录
     * @param reservationNumber 预约单号
     * @return com.check.pojo.MedicalRecord
     */
    MedicalRecord getRecordsByReservationNumber(String reservationNumber);

    /**
     * 更新医疗记录信息
     * @param medicalRecord com.check.pojo.MedicalRecord
     */
    void updateMedicalRecord(MedicalRecord medicalRecord);

    /**
     * 获取历史记录信息
     * @param doctorNumber 医生编号
     * @return com.check.pojo.MedicalNumber
     */
    List<MedicalRecord> getRecordsByDoctorNumber(String doctorNumber);

}
