package com.check.mapper;

import com.check.pojo.ProjectPrice;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface ProjectPriceMapper {

    /**
     * 查询一项的价格
     * @param project 项目
     * @return com.check.pojo.ProjectPrice
     */
    ProjectPrice queryByppProject(String project);


    /**
     * 通过ID查询单条数据
     *
     * @param ppId 主键
     * @return 实例对象
     */
    ProjectPrice queryById(Integer ppId);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<ProjectPrice> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param projectPrice 实例对象
     * @return 对象列表
     */
    List<ProjectPrice> queryAll(ProjectPrice projectPrice);

    /**
     * 新增数据
     *
     * @param projectPrice 实例对象
     * @return 影响行数
     */
    int insert(ProjectPrice projectPrice);

    /**
     * 修改数据
     *
     * @param projectPrice 实例对象
     * @return 影响行数
     */
    int update(ProjectPrice projectPrice);

    /**
     * 通过主键删除数据
     *
     * @param ppId 主键
     * @return 影响行数
     */
    int deleteById(Integer ppId);

    ProjectPrice selectByppProject(String project);
}
