package com.check.mapper;

import com.check.pojo.Doctor;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface DoctorMapper {

    /**
     * 获取医生信息
     * @param openid 唯一标识
     * @return com.check.pojo.Doctor
     */
    Doctor getDoctorInfo(String openid);

    /**
     * 获取医生信息
     * @param doctorNumber 医生编号
     * @return com.check.pojo.Doctor
     */
    Doctor getDoctorInfoByDoctorNumber(String doctorNumber);

    /**
     * 获取所有医生的信息
     * @return 医生账号，医生姓名，医生手机号码，医生身份证号，性别，医生职称
     */
    List<Doctor> getAllDoctorInfo();

    /**
     * 更新doctor中携带的信息
     * @param doctor com.check.pojo.Doctor
     * @return
     */
    int updateDoctor(Doctor doctor);

    /**
     * 获取预约的日期列表
     * @param doctorOpenid 唯一标识
     * @return 日期列表
     */
    List<String> getBookScheduleDateList(String doctorOpenid);

    /**
     * 获取当天的时间列表
     * @param doctorOpenid 唯一标识
     * @param bookDate 预约日期
     * @return 时间列表
     */
    List<String> getBookScheduleTime(@Param("doctorOpenid") String doctorOpenid, @Param("bookDate") String bookDate);

    /**
     * 增加医生表信息
     * @param doctor
     * @return
     */
    int insertDoctor(Doctor doctor);
}
