package com.check.mapper;

import com.check.pojo.SelectProject;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * (SelectProject)表数据库访问层
 *
 * @author makejava
 * @since 2022-05-09 18:26:50
 */
@Mapper
@Repository
public interface SelectProjectMapper {

    List<Integer> getProjectId(@Param("openid") String openid, @Param("paytime") String paytime);

    List<String> getPayTime(String openid);

    int insertProjects(@Param("projectname") String projectname, @Param("openid") String openid, @Param("paytime") String paytime);

    List<SelectProject> getProjectEntity(@Param("openid") String openid, @Param("paytime") String paytime);

    List<SelectProject> selectInspectionItemByProject(@Param("openid") String openid, @Param("paytime") String paytime);
}

