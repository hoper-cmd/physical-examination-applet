package com.check.mapper;

import com.check.pojo.BookSchedule;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface BookScheduleMapper {

    /**
     * 新增医生预约信息
     * @param bookSchedule 医生预约信息
     */
    void insertBookSchedule(BookSchedule bookSchedule);

    /**
     * 查询预约时间晚于当前时间的医生
     * @param currentDate 当前时间
     * @return 医生唯一标识的集合
     */
    List<String> getDoctorsAfterCurrentDate(String currentDate);

    /**
     * 查询同一位医生下，晚于当前日期的日期
     * @param doctorOpenid 医生唯一标识
     * @param currentDate 当前日期
     * @return 日期的集合
     */
    List<String> getDateByDoctorOpenidAndCurrentDate(@Param("doctorOpenid") String doctorOpenid, @Param("currentDate") String currentDate);

    /**
     * 查询同一位医生，在同一日期下，晚于当前时间的时间
     * @param date 固定日期
     * @param doctorOpenid 医生唯一标识
     * @param currentTime 当前时间
     * @return 时间的集合
     */
    List<String> getTimesByDateAndDoctorOpenid(@Param("date") String date,
                                               @Param("doctorOpenid") String doctorOpenid,
                                               @Param("currentTime") String currentTime,
                                               @Param("currentDate") String currentDate);

    /**
     * 修改预约时间表
     * @param bookSchedule 预约时间表
     */
    void updateBookSchedule(BookSchedule bookSchedule);
}
