package com.check.mapper;

import com.check.pojo.BodySymptoms;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface BodySymptomsMapper {
    /**
     * 根据选项查询记录
     * @param titleNum 题目编号
     * @return 符合条件的选项集
     */
    List<BodySymptoms> getBodySymptomsByTitleNum(@Param("titleNum") String titleNum);

    /**
     * 根据标题和选项确定唯一元素
     * @param titleNum 题目编号
     * @param option 选项内容
     * @return 符合条件的一条记录
     */
    BodySymptoms getBodySymptomsByTitleNumAndOption(@Param("titleNum") String titleNum,
                                                          @Param("option") String option);

    /**
     * 查询推荐的体检项目
     * @param titleNum 题目编号
     * @param option 选项内容
     * @param lowAge 最低年龄
     * @param sex 性别
     * @return 符合条件的体检项目集合
     */
    List<String> getScreeningProgramme(@Param("titleNum") String titleNum,
                                       @Param("option") String option,
                                       @Param("lowAge") Integer lowAge,
                                       @Param("sex") String sex);

    /**
     * 查询记录
     * @param BSId 表主键
     * @return 符合条件的一条记录
     */
    BodySymptoms getBodySymptomsByHMId(@Param("BSId") Integer BSId);
}
