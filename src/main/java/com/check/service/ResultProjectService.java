package com.check.service;

import com.check.pojo.ResultProject;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ResultProjectService {


    ResultProject queryById(Integer rpId);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<ResultProject> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param resultProject 实例对象
     * @return 实例对象
     */
    ResultProject insert(ResultProject resultProject);

    /**
     * 修改数据
     *
     * @param resultProject 实例对象
     * @return 实例对象
     */
    ResultProject update(ResultProject resultProject);

    /**
     * 通过主键删除数据
     *
     * @param rpId 主键
     * @return 是否成功
     */
    boolean deleteById(Integer rpId);

    List selectAllResult();

    List<ResultProject> selectAll();

    List selectAllProject();
    ResultProject queryByResult(String result);
    List queryProjectByOptions(String[] options);
}