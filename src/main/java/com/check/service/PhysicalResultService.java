package com.check.service;

import com.check.pojo.PhysicalResult;

import java.util.List;

/**
 * (PhysicalResult)表服务接口
 *
 * @author makejava
 * @since 2022-05-10 15:27:34
 */
public interface PhysicalResultService {

    int insertResort(PhysicalResult physicalResult);

    List<Integer> getInspectionIds(Integer projectid);

    List<String> selectNormal(Integer projectid, Integer inspectionid);
}
