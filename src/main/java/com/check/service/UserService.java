package com.check.service;

import org.springframework.stereotype.Repository;
import java.util.Map;

@Repository
public interface UserService {

    // 登陆接口
    Map<String, Object> wxLogin(String code);
}
