package com.check.service;

import com.check.base.Result;
import com.check.pojo.HabitDiet;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.Set;

@Repository
public interface HabitDietService {
    /**
     * 根据选项查询记录
     * @param titleNum 题目编号
     * @return 符合条件的选项集
     */
    Result<HabitDiet> getHabitDietByTitleNum(@Param("titleNum") String titleNum);

    /**
     * 根据标题和选项确定唯一元素
     * @param titleNum 题目编号
     * @param option 选项内容
     * @return 符合条件的一条记录
     */
    Result<HabitDiet> getHabitDietByTitleNumAndOption(@Param("titleNum") String titleNum,
                                                        @Param("option") String option);


    /**
     * 查询记录
     * @param HDId 表主键
     * @return 符合条件的一条记录
     */
    Result<HabitDiet> getHabitDietByHDId(@Param("HDId") Integer HDId);

    /**
     * 获取体检推荐结果
     * @param map 携带参数信息的集
     * @return 体检推荐结果的去重集
     */
    Set<String> recommendScreeningProgramme(Map map);
}
