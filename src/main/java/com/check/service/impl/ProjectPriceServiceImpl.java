package com.check.service.impl;

import com.check.mapper.ProjectPriceMapper;
import com.check.pojo.ProjectPrice;
import com.check.service.ProjectPriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("projectPriceService")
public class ProjectPriceServiceImpl implements ProjectPriceService {
    @Autowired
    private ProjectPriceMapper projectPriceMapper;

    /**
     * 通过ID查询单条数据
     *
     * @param ppId 主键
     * @return 实例对象
     */
    @Override
    public ProjectPrice queryById(Integer ppId) {
        return this.projectPriceMapper.queryById(ppId);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<ProjectPrice> queryAllByLimit(int offset, int limit) {
        return this.projectPriceMapper.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param projectPrice 实例对象
     * @return 实例对象
     */
    @Override
    public ProjectPrice insert(ProjectPrice projectPrice) {
        this.projectPriceMapper.insert(projectPrice);
        return projectPrice;
    }

    /**
     * 修改数据
     *
     * @param projectPrice 实例对象
     * @return 实例对象
     */
    @Override
    public ProjectPrice update(ProjectPrice projectPrice) {
        this.projectPriceMapper.update(projectPrice);
        return this.queryById(projectPrice.getPpId());
    }

    /**
     * 通过主键删除数据
     *
     * @param ppId 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer ppId) {
        return this.projectPriceMapper.deleteById(ppId) > 0;
    }

    @Override
    public Map<String, List> queryByppProject(String[] ppProjects) {
        List<ProjectPrice> multiple_list = new ArrayList<>();
        List redio_list = new ArrayList<>();

        Map<String, List> objectMap = new HashMap<>();

        for (int i = 0; i < ppProjects.length; i++) {
            if (ppProjects[i].indexOf("或") < 0) {
                multiple_list.add(this.projectPriceMapper.queryByppProject(ppProjects[i]));
            } else {
                String[] split = ppProjects[i].split("或");
                if (split != null && split.length != 0) {
                    List<ProjectPrice> redio_m = new ArrayList();
                    for (int j = 0; j < split.length; j++) {
                        redio_m.add(this.projectPriceMapper.queryByppProject(split[j]));  //向集合中添加对象
                    }
                    System.out.println("一组单选" + redio_m);
                    redio_list.add(redio_m);
                }
            }
        }
        System.out.println("全部组单选" + redio_list);
        objectMap.put("multiple", multiple_list);
        objectMap.put("redio", redio_list);
        return objectMap;
    }

    @Override
    public ProjectPrice selectByppProject(String project){
        return this.projectPriceMapper.selectByppProject(project);
    }

}