package com.check.service.impl;

import com.check.mapper.ResultProjectMapper;
import com.check.pojo.ResultProject;
import com.check.service.ResultProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class ResultProjectServiceImpl implements ResultProjectService {
    @Autowired
    private ResultProjectMapper resultProjectMapper;
    private ResultProject resultProject;
    /**
     * 通过ID查询单条数据
     *
     * @param rpId 主键
     * @return 实例对象
     */
    @Override
    public ResultProject queryById(Integer rpId) {
        return this.resultProjectMapper.queryById(rpId);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<ResultProject> queryAllByLimit(int offset, int limit) {
        return this.resultProjectMapper.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param resultProject 实例对象
     * @return 实例对象
     */
    @Override
    public ResultProject insert(ResultProject resultProject) {
        this.resultProjectMapper.insert(resultProject);
        return resultProject;
    }

    /**
     * 修改数据
     *
     * @param resultProject 实例对象
     * @return 实例对象
     */
    @Override
    public ResultProject update(ResultProject resultProject) {
        this.resultProjectMapper.update(resultProject);
        return this.queryById(resultProject.getRpId());
    }

    /**
     * 通过主键删除数据
     *
     * @param rpId 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer rpId) {
        return this.resultProjectMapper.deleteById(rpId) > 0;
    }

    @Override
    public List selectAllResult() {
        return this.resultProjectMapper.selectAllResult();
    }

    @Override
    public List<ResultProject> selectAll() {
        return this.resultProjectMapper.selectAll();
    }

    @Override
    public List selectAllProject() {
        return this.resultProjectMapper.selectAllProject();
    }

    @Override
    public ResultProject queryByResult(String result) {
        return this.resultProjectMapper.queryByResult(result);
    }

    @Override
    public List queryProjectByOptions(String[] options) {

        List<String> project=new ArrayList<String>();
        Set set = new HashSet();  //定义Set集合对象
        for (int i = 0; i < options.length; i++) {
            String option= options[i];
            resultProject = queryByResult(option);
            String projectStr=resultProject.getProject();
            String[] split = projectStr.split("、");
            if (split != null && split.length != 0) {
                for (int j = 0; j < split.length; j++) {
                    set.add(split[j]);  //向集合中添加对象
                }
            }
        }
        List<String> pro_list = new ArrayList<>(set);
        return pro_list;
    }

}