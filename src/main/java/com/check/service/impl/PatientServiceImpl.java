package com.check.service.impl;

import com.check.base.Result;
import com.check.mapper.*;
import com.check.pojo.*;
import com.check.pojo.dto.PatientDTO;
import com.check.service.PatientService;
import com.check.util.IdGenerateUtil;
import com.check.util.TimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service("patientService")
public class PatientServiceImpl implements PatientService {

    @Autowired
    private PatientMapper patientMapper;

    @Autowired
    private DoctorMapper doctorMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private BookScheduleMapper bookScheduleMapper;

    @Autowired
    private ReservationMapper reservationMapper;

    @Autowired
    private MedicalRecordMapper medicalRecordMapper;

    @Autowired
    private ResultProjectMapper resultProjectMapper;

    @Autowired
    private ProjectPriceMapper projectPriceMapper;

    @Override
    public List<Map<String, Object>> getAllBookDoctors(Integer currentTab) {
        List<Map<String,Object>> list = new ArrayList<>();

        // 当前的日期和时间
        String currentDate = TimeUtil.getCurrentDate();
        String currentTime = TimeUtil.getCurrentTimeWithoutSeconds();

        // 查找比当前日期晚的医生openid
        List<String> doctors = bookScheduleMapper.getDoctorsAfterCurrentDate(currentDate);

        // 取出各个医生的医生的基本信息
        for (String doctorOpenid : doctors) {
            Map<String,Object> map = new HashMap<>();

            Doctor doctorInfo = doctorMapper.getDoctorInfo(doctorOpenid);
            User userInfo = userMapper.getUserByOpenid(doctorOpenid);

            String doctorName = doctorInfo.getDoctorName();
            String doctorPhoneNumber = userInfo.getPhoneNumber();
            String doctorTitle = doctorInfo.getDoctorTitle();
            String hospitalSupports = doctorInfo.getHospitalSupports();
            String hospitalDepartment = doctorInfo.getHospitalDepartment();

            // 获取有号的日期和时间
            List<Map<String,Object>> treatedTimes = new ArrayList<>();
            List<String> dates = bookScheduleMapper.getDateByDoctorOpenidAndCurrentDate(doctorOpenid,currentDate);
            if(currentTab == 0){
                for (String date : dates) {
                    if(date.equals(currentDate)){
                        Map<String,Object> timesMap = new HashMap<>();
                        // 当前日期下，比当前时间晚的时间集合
                        List<String> times = bookScheduleMapper.getTimesByDateAndDoctorOpenid(date,doctorOpenid,currentTime,currentDate);
                        // 去除掉为空值的time
                        times.removeIf(time -> null == time || "".equals(time));
                        timesMap.put("date",date);
                        timesMap.put("times",times);
                        treatedTimes.add(timesMap);
                    }
                }
            }else {
                for (String date : dates) {
                    Map<String,Object> timesMap = new HashMap<>();
                    // 当前日期下，比当前时间晚的时间集合
                    List<String> times = bookScheduleMapper.getTimesByDateAndDoctorOpenid(date,doctorOpenid,currentTime,currentDate);
                    // 去除掉为空值的time
                    times.removeIf(time -> null == time || "".equals(time));
                    timesMap.put("date",date);
                    timesMap.put("times",times);
                    treatedTimes.add(timesMap);
                }
            }
            map.put("doctorName",doctorName);
            map.put("doctorPhoneNumber",doctorPhoneNumber);
            map.put("doctorTitle",doctorTitle);
            map.put("hospitalSupports",hospitalSupports);
            map.put("hospitalDepartment",hospitalDepartment);
            map.put("treatedTimes",treatedTimes);

            list.add(map);
        }
        return list;
    }

    @Override
    public List<Map<String, Object>> getRecordsByPatientOpenid(String patientOpenid) {

        List<Map<String,Object>> list = new ArrayList<>();

        // 获取患者信息
        Patient patientInfo = patientMapper.getPatientInfo(patientOpenid);
        String patientName = patientInfo.getPatientName();
        String patientNumber = patientInfo.getPatientNumber();

        List<MedicalRecord> medicalRecordList = medicalRecordMapper.getRecordsByPatientNumber(patientNumber);
        for (MedicalRecord medicalRecord : medicalRecordList) {
            // 医生姓名，就诊院区，就诊科室
            String doctorNumber = medicalRecord.getDoctorNumber();
            Doctor doctorInfo = doctorMapper.getDoctorInfoByDoctorNumber(doctorNumber);
            String doctorName = doctorInfo.getDoctorName();
            String hospitalSupports = doctorInfo.getHospitalSupports();
            String hospitalDepartment = doctorInfo.getHospitalDepartment();

            // 医生手机号
            String doctorOpenid = doctorInfo.getDoctorOpenid();
            String doctorPhoneNumber = userMapper.getUserByOpenid(doctorOpenid).getPhoneNumber();

            // 预约时间
            Reservation reservation = reservationMapper.getReservation(medicalRecord.getReservationNumber());
            String reservationStartTime = reservation.getReservationStartTime();
            String reservationNumber = reservation.getReservationNumber();

            Map<String,Object> map = new HashMap<>();
            map.put("reservationNumber",reservationNumber);
            map.put("doctorName",doctorName);
            map.put("hospitalSupports",hospitalSupports);
            map.put("hospitalDepartment",hospitalDepartment);
            map.put("doctorPhoneNumber",doctorPhoneNumber);
            map.put("patientName",patientName);
            map.put("reservationStartTime",reservationStartTime);

            list.add(map);
        }
        return list;
    }

    @Override
    public List<Map<String, Object>> getAllPatientsInfo() {
        List<Map<String,Object>> list = new ArrayList<>();
        List<Patient> patientList = patientMapper.getAllPatientInfo();
        for (Patient patient : patientList) {
            // 患者编号，患者姓名，患者手机号码，患者身份证号，性别，职业，民族，婚姻状况，受教育程度
            String patientNumber = patient.getPatientNumber();
            String patientName = patient.getPatientName();
            String patientJob = patient.getPatientJob();
            String marryCondition = patient.getMarryCondition();
            String educationExtend = patient.getEducationExtend();

            User user = userMapper.getUserByOpenid(patient.getPatientOpenid());
            String phoneNumber = user.getPhoneNumber();
            String idcardNumber = user.getIdcardNumber();
            String sex = user.getSex();
            String nation = user.getNation();

            Map<String,Object> map = new HashMap<>();
            map.put("patientNumber",patientNumber);
            map.put("patientName",patientName);
            map.put("patientJob",patientJob);
            map.put("marryCondition",marryCondition);
            map.put("educationExtend",educationExtend);
            map.put("phoneNumber",phoneNumber);
            map.put("idcardNumber",idcardNumber);
            map.put("sex",sex);
            map.put("nation",nation);

            list.add(map);
        }
        return list;
    }

    @Override
    public Result editPatient(PatientDTO patientDTO) {
        Patient patientInfo = patientMapper.getPatientInfoByPatientNumber(patientDTO.getPatientNumber());
        int result1 = 0;
        int result2 = 0;
        Map<String,Object> resultMap = new HashMap<>();
        if(patientInfo == null){
            String openid = patientDTO.getOpenid();
            User user = new User();
            user.setOpenid(openid);
            user.setRealName(patientDTO.getRealName());
            user.setPhoneNumber(patientDTO.getPhoneNumber());
            user.setIdcardNumber(patientDTO.getIdcardNumber());
            user.setSex(patientDTO.getSex());
            user.setBirth(patientDTO.getBirth());
            user.setLivingPlace(patientDTO.getLivingPlace());
            user.setNation(patientDTO.getNation());
            result1 = userMapper.updateUser(user);

            Patient patient = new Patient();
            List<Patient> patientList = patientMapper.getAllPatientInfo();
            int size = patientList.size();
            int patientId = 0;
            if(size != 0){
                patientId = patientList.get(size-1).getPatientId();
            }
            patient.setPatientNumber(IdGenerateUtil.NumberGenerate(1,patientId));
            patient.setPatientName(patientDTO.getPatientName());
            patient.setPatientOpenid(openid);
            patient.setPatientJob(patientDTO.getPatientJob());
            patient.setMarryCondition(patientDTO.getMarryCondition());
            patient.setEducationExtend(patientDTO.getEducationExtend());

            result2 = patientMapper.insertPatient(patient);

            resultMap.put("patientNumber",patient.getPatientNumber());
            resultMap.put("patientId",patient.getPatientId());

        }else {
            String patientOpenid = patientInfo.getPatientOpenid();

            User user = new User();
            user.setRealName(patientDTO.getRealName());
            user.setOpenid(patientOpenid);
            user.setPhoneNumber(patientDTO.getPhoneNumber());
            user.setIdcardNumber(patientDTO.getIdcardNumber());
            user.setSex(patientDTO.getSex());
            user.setBirth(patientDTO.getBirth());
            user.setLivingPlace(patientDTO.getLivingPlace());
            user.setNation(patientDTO.getNation());
            result1 = userMapper.updateUser(user);

            Patient patient = new Patient();
            patient.setPatientId(patientDTO.getPatientId());
            patient.setPatientNumber(patientDTO.getPatientNumber());
            patient.setPatientName(patientDTO.getPatientName());
            patient.setPatientOpenid(patientOpenid);
            patient.setPatientJob(patientDTO.getPatientJob());
            patient.setMarryCondition(patientDTO.getMarryCondition());
            patient.setEducationExtend(patientDTO.getEducationExtend());
            result2 = patientMapper.updatePatient(patient);

            resultMap.put("patientNumber",patient.getPatientNumber());
            resultMap.put("patientId",patient.getPatientId());

        }
        if(result1 + result2 >= 2){
            return Result.success(200,"修改成功",resultMap);
        }else {
            return Result.failure(0,"修改失败，请联系运维人员");
        }
    }

    @Override
    public Result insertReservation(String patientOpenid, String doctorPhoneNumber, String bookDateAndTime) {

        Patient patientInfo = patientMapper.getPatientInfo(patientOpenid);
        String patientNumber = patientInfo.getPatientNumber();
        User userInfo = userMapper.getUserByPhoneNumber(doctorPhoneNumber);
        String doctorOpenid = userInfo.getOpenid();
        Doctor doctorInfo = doctorMapper.getDoctorInfo(doctorOpenid);
        String doctorNumber = doctorInfo.getDoctorNumber();
        String hospitalSupports = doctorInfo.getHospitalSupports();
        String hospitalDepartment = doctorInfo.getHospitalDepartment();

        Reservation rea = reservationMapper.queryReservation(patientNumber,doctorNumber,bookDateAndTime);
        if(null == rea){
            // 构建Reservation对象
            Reservation reservation = new Reservation();
            reservation.setReservationNumber(IdGenerateUtil.IdGenerate());
            reservation.setPatientNumber(patientNumber);
            reservation.setDoctorNumber(doctorNumber);
            reservation.setTreatedCampus(hospitalSupports);
            reservation.setTreatedDepartment(hospitalDepartment);
            reservation.setReservationCreatedTime(TimeUtil.getCurrentTimeWithoutSeconds());
            reservation.setReservationStartTime(bookDateAndTime);

            int result1 = reservationMapper.insertReservation(reservation);

            // 创建对象信息
            MedicalRecord medicalRecord = new MedicalRecord();
            medicalRecord.setDoctorNumber(doctorNumber);
            medicalRecord.setPatientNumber(patientNumber);
            medicalRecord.setTreatedStartTime(bookDateAndTime);
            medicalRecord.setReservationNumber(reservation.getReservationNumber());

            // 添加进记录当中
            int result2 = medicalRecordMapper.insertMedicalRecord(medicalRecord);

            if(result1 + result2 >= 2){
                return Result.success(200,"添加预约单成功");
            }else {
                return Result.failure(0,"添加预约单失败,请联系运维人员");
            }
        }else {
            return Result.success(200,"您已经创建过此预约单,请不要重复添加");
        }
    }

    @Override
    public void endThisReservation(String reservationNumber) {
        // 获取当前时间，如果预约时间晚于当前时间，取消预;反之，结束预约
        Reservation reservation = reservationMapper.getReservation(reservationNumber);
        String reservationStartTime = reservation.getReservationStartTime();
        boolean flag = judgeDateAndTime(reservationStartTime);
        if(flag){
            reservation.setIsCanceled(1);
        }else {
            reservation.setIsEnded(1);
        }
        String currentTimeWithoutSeconds = TimeUtil.getCurrentTimeWithoutSeconds();
        reservation.setReservationEndedTime(currentTimeWithoutSeconds);
        reservationMapper.updateReservation(reservation);

        // 同时更新记录表
        MedicalRecord medicalRecord  = medicalRecordMapper.getRecordsByReservationNumber(reservationNumber);
        medicalRecord.setTreatedEndedTime(currentTimeWithoutSeconds);
        medicalRecordMapper.updateMedicalRecord(medicalRecord);
    }

    @Override
    public List<ResultProject> selectAllResult() {
        return resultProjectMapper.selectAllResult();
    }

    @Override
    public List<String> queryProjectByOptions(String[] options) {
        List<String> project=new ArrayList<String>();
        Set<String> set = new HashSet<>();  //定义Set集合对象
        for (String option : options) {
            ResultProject resultProject = resultProjectMapper.queryByResult(option);
            String projectStr = resultProject.getProject();
            String[] split = projectStr.split("、");
            if (split.length != 0) {
                Collections.addAll(set, split);
            }
        }
        return new ArrayList<>(set);
    }

    @Override
    public Map<String, List> queryByppProject(String[] checkProject) {
        List<ProjectPrice> multiple_list = new ArrayList<>();
        List redio_list = new ArrayList<>();

        Map<String, List> objectMap = new HashMap<>();

        for (String value : checkProject) {
            if (!value.contains("或")) {
                multiple_list.add(projectPriceMapper.queryByppProject(value));
            } else {
                String[] split = value.split("或");
                if (split.length != 0) {
                    List<ProjectPrice> redio_m = new ArrayList();
                    for (String s : split) {
                        redio_m.add(projectPriceMapper.queryByppProject(s));  //向集合中添加对象
                    }
                    System.out.println("一组单选" + redio_m);
                    redio_list.add(redio_m);
                }
            }
        }
        System.out.println("全部组单选" + redio_list);
        objectMap.put("multiple", multiple_list);
        objectMap.put("redio", redio_list);
        return objectMap;
    }

    @Override
    public Result showPatientInfo(String patientOpenid) {
        Patient patientInfo = patientMapper.getPatientInfo(patientOpenid);
        if(null != patientInfo){
            User user = userMapper.getUserByOpenid(patientOpenid);
            String realName = user.getRealName();
            String idcardNumber = user.getIdcardNumber();
            String phoneNumber = user.getPhoneNumber();
            String patientJob = patientInfo.getPatientJob();
            String sex = user.getSex();
            String livingPlace = user.getLivingPlace();
            String nation = user.getNation();
            String educationExtend = patientInfo.getEducationExtend();
            String marryCondition = patientInfo.getMarryCondition();
            String birth = user.getBirth();
            String patientNumber = patientInfo.getPatientNumber();
            Integer patientId = patientInfo.getPatientId();

            Map<String,Object> map = new HashMap<>();
            map.put("realName",realName);
            map.put("idcardNumber",idcardNumber);
            map.put("phoneNumber",phoneNumber);
            map.put("patientJob",patientJob);
            map.put("sex",sex);
            map.put("livingPlace",livingPlace);
            map.put("nation",nation);
            map.put("educationExtend",educationExtend);
            map.put("marryCondition",marryCondition);
            map.put("birth",birth);
            map.put("patientNumber",patientNumber);
            map.put("patientId",patientId);

            return Result.success(200,"成功",map);
        }else {
            return Result.failure(0,"无该患者数据");
        }
    }


    /**
     * 时间字符串判断
     * @param str 字符串
     * @return true 前者大于后者（前者晚于后者） false-前者小于后者（前者早于后者）
     */
    private boolean judgeDateAndTime(String str){
        String year = str.substring(0, 4);
        String month = str.substring(5, 7);
        String date = str.substring(8, 10);
        String hour = str.substring(11,13);
        String minute = str.substring(14, 16);

        String currentYear = TimeUtil.getCurrentTime("yyyy");
        String currentMonth = TimeUtil.getCurrentTime("MM");
        String currentDate = TimeUtil.getCurrentTime("dd");
        String currentHour = TimeUtil.getCurrentTime("HH");
        String currentMinute = TimeUtil.getCurrentTime("mm");

        if(judge(year,currentYear))
            return true;
        else if(judge(currentYear,year))
            return false;
        else {
            if(judge(month,currentMonth))
                return true;
            else if(judge(currentMonth,month))
                    return false;
            else {
                if (judge(date,currentDate))
                    return true;
                else if (judge(currentDate,date))
                    return false;
                else {
                    if (judge(hour,currentHour))
                        return true;
                    else if (judge(currentHour,hour))
                        return false;
                    else{
                        return judge(minute, currentMinute);
                    }
                }
            }

        }
    }

    /**
     * 字符串大小判断
     * @param str1 字符串1
     * @param str2 字符串2
     * @return true-晚于 false-早于
     */
    private boolean judge(String str1, String str2){
        int number1 = Integer.parseInt(str1);
        int number2 = Integer.parseInt(str2);
        return number1 > number2;
    }
}
