package com.check.service.impl;

import com.check.base.Result;
import com.check.mapper.*;
import com.check.pojo.*;
import com.check.pojo.dto.CommandDTO;
import com.check.service.CommandService;
import com.check.util.TimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("commandService")
public class CommandServiceImpl implements CommandService {

    @Autowired
    private ReservationMapper reservationMapper;

    @Autowired
    private PatientMapper patientMapper;

    @Autowired
    private DoctorMapper doctorMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserRoleMapper userRoleMapper;

    @Autowired
    private MedicalRecordMapper medicalRecordMapper;

    @Override
    public List<Map<String, Object>> getAllReservations() {
        List<Map<String, Object>> list = new ArrayList<>();

        List<Reservation> reservationList = reservationMapper.getAllReservation();
        for (Reservation reservation : reservationList) {
            String reservationEndedTime = reservation.getReservationEndedTime();
            if("".equals(reservationEndedTime) || null == reservationEndedTime){
                // 获取预约单信息
                String reservationNumber = reservation.getReservationNumber();
                String patientNumber = reservation.getPatientNumber();
                String reservationStartTime = reservation.getReservationStartTime();
                String treatedCampus = reservation.getTreatedCampus();
                String treatedDepartment = reservation.getTreatedDepartment();
                String doctorNumber = reservation.getDoctorNumber();

                // 获取医生信息
                Doctor doctorInfo = doctorMapper.getDoctorInfoByDoctorNumber(doctorNumber);
                String doctorName = doctorInfo.getDoctorName();
                String doctorTitle = doctorInfo.getDoctorTitle();
                String doctorOpenid = doctorInfo.getDoctorOpenid();
                User doctorUser = userMapper.getUserByOpenid(doctorOpenid);
                String doctorPhoneNumber = doctorUser.getPhoneNumber();

                // 获取患者信息
                Patient patientInfo = patientMapper.getPatientInfoByPatientNumber(patientNumber);
                String patientName = patientInfo.getPatientName();
                String patientOpenid = patientInfo.getPatientOpenid();
                User patientUser = userMapper.getUserByOpenid(patientOpenid);
                String patientPhoneNumber = patientUser.getPhoneNumber();

                // 创建map 保存一次的对象
                Map<String,Object> map = new HashMap<>();
                map.put("reservationNumber",reservationNumber);
                map.put("patientNumber",patientNumber);
                map.put("patientName",patientName);
                map.put("patientPhoneNumber",patientPhoneNumber);
                map.put("reservationStartTime",reservationStartTime);
                map.put("treatedCampus",treatedCampus);
                map.put("treatedDepartment",treatedDepartment);
                map.put("doctorName",doctorName);
                map.put("doctorPhoneNumber",doctorPhoneNumber);
                map.put("doctorTitle",doctorTitle);

                list.add(map);
            }
        }
        return list;
    }

    @Override
    public List<String> getCommandAccount() {
        return userRoleMapper.getAccounts(3);
    }

    @Override
    public void endThisReservation(String reservationNumber) {
        // 获取当前时间，如果预约时间晚于当前时间，取消预;反之，结束预约
        Reservation reservation = reservationMapper.getReservation(reservationNumber);
        String reservationStartTime = reservation.getReservationStartTime();
        boolean flag = judgeDateAndTime(reservationStartTime);
        if(flag){
            reservation.setIsCanceled(1);
        }else {
            reservation.setIsEnded(1);
        }
        String currentTimeWithoutSeconds = TimeUtil.getCurrentTimeWithoutSeconds();
        reservation.setReservationEndedTime(currentTimeWithoutSeconds);
        reservationMapper.updateReservation(reservation);

        // 同时更新记录表
        MedicalRecord medicalRecord  = medicalRecordMapper.getRecordsByReservationNumber(reservationNumber);
        medicalRecord.setTreatedEndedTime(currentTimeWithoutSeconds);
        medicalRecordMapper.updateMedicalRecord(medicalRecord);
    }

    @Override
    public int editThisReservation(Reservation reservation) {
        return reservationMapper.updateReservation(reservation);
    }

    @Override
    public Result editPersonalInfo(CommandDTO commandDTO) {
        String commandAccount = commandDTO.getCommandAccount();
        UserRole userRole = userRoleMapper.getInfoByAccount(commandAccount);
        String openid = userRole.getOpenid();

        User user = new User();
        user.setOpenid(openid);
        user.setRealName(commandDTO.getRealName());
        user.setPhoneNumber(commandDTO.getPhoneNumber());
        user.setIdcardNumber(commandDTO.getIdcardNumber());
        user.setLivingPlace(commandDTO.getLivingPlace());
        user.setSex(commandDTO.getSex());
        user.setBirth(commandDTO.getBirth());
        user.setNation(commandDTO.getNation());
        int result = userMapper.updateUser(user);
        if(result > 0){
            return Result.success(200,"修改成功");
        }else {
            return Result.failure(0,"修改失败,请联系运维人员");
        }
    }

    @Override
    public Result showCommandInfo(String commandAccount) {
        UserRole userRole = userRoleMapper.getInfoByAccount(commandAccount);
        String openid = userRole.getOpenid();
        User user = userMapper.getUserByOpenid(openid);

        String realName = user.getRealName();
        String idcardNumber = user.getIdcardNumber();
        String phoneNumber = user.getPhoneNumber();
        String nation = user.getNation();
        String birth = user.getBirth();
        String sex = user.getSex();
        String livingPlace = user.getLivingPlace();

        Map<String,Object> map = new HashMap<>();
        map.put("realName",realName);
        map.put("idcardNumber",idcardNumber);
        map.put("phoneNumber",phoneNumber);
        map.put("nation",nation);
        map.put("birth",birth);
        map.put("sex",sex);
        map.put("livingPlace",livingPlace);

        return Result.success(200,"成功",map);
    }

    /**
     * 时间字符串判断
     * @param str 字符串
     * @return true 前者大于后者（前者晚于后者） false-前者小于后者（前者早于后者）
     */
    private boolean judgeDateAndTime(String str){
        String year = str.substring(0, 4);
        String month = str.substring(5, 7);
        String date = str.substring(8, 10);
        String hour = str.substring(11,13);
        String minute = str.substring(14, 16);

        String currentYear = TimeUtil.getCurrentTime("yyyy");
        String currentMonth = TimeUtil.getCurrentTime("MM");
        String currentDate = TimeUtil.getCurrentTime("dd");
        String currentHour = TimeUtil.getCurrentTime("HH");
        String currentMinute = TimeUtil.getCurrentTime("mm");

        if(judge(year,currentYear))
            return true;
        else if(judge(currentYear,year))
            return false;
        else {
            if(judge(month,currentMonth))
                return true;
            else if(judge(currentMonth,month))
                return false;
            else {
                if (judge(date,currentDate))
                    return true;
                else if (judge(currentDate,date))
                    return false;
                else {
                    if (judge(hour,currentHour))
                        return true;
                    else if (judge(currentHour,hour))
                        return false;
                    else{
                        return judge(minute, currentMinute);
                    }
                }
            }

        }
    }

    /**
     * 字符串大小判断
     * @param str1 字符串1
     * @param str2 字符串2
     * @return true-晚于 false-早于
     */
    private boolean judge(String str1, String str2){
        int number1 = Integer.parseInt(str1);
        int number2 = Integer.parseInt(str2);
        return number1 > number2;
    }
}
