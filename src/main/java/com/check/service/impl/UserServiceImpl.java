package com.check.service.impl;

import com.alibaba.druid.util.StringUtils;
import com.alibaba.fastjson.JSONObject;
import com.check.mapper.PatientMapper;
import com.check.mapper.UserMapper;
import com.check.mapper.UserRoleMapper;
import com.check.pojo.Patient;
import com.check.pojo.User;
import com.check.pojo.UserRole;
import com.check.pojo.dto.PatientDTO;
import com.check.service.PatientService;
import com.check.service.UserService;
import com.check.util.ConstUtil;
import com.check.util.IdGenerateUtil;
import com.check.util.TimeUtil;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("userService")
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserRoleMapper userRoleMapper;

    @Autowired
    private PatientService patientService;

    @Override
    public Map<String, Object> wxLogin(String code) {
        // 返回code
        System.out.println("code的值：" + code);
        Map<String, Object> resultMap = new HashMap<>();
        String appid = ConstUtil.APP_ID;
        String secret = ConstUtil.APP_SECRET;

        // 拼接sql
        String loginUrl = "https://api.weixin.qq.com/sns/jscode2session?appid=" + appid +
                "&secret=" + secret + "&js_code=" + code + "&grant_type=authorization_code";

        CloseableHttpClient client = null;
        CloseableHttpResponse response = null;

        try{
            // 创建httpGet请求
            HttpGet httpGet = new HttpGet(loginUrl);
            // 发送请求
            client = HttpClients.createDefault();
            // 执行请求
            response = client.execute(httpGet);
            // 得到返回数据
            HttpEntity entity = response.getEntity();

            String result = EntityUtils.toString(entity);
            System.out.println("微信返回的结果" + result);

            resultMap.put("data", result);

            // 对返回的结果进行解析
            JSONObject json_test = JSONObject.parseObject(result);
            System.out.println("-------------------------------------------------");
            System.out.println(json_test);
            System.out.println("-------------------------------------------------");
            String openid = json_test.getString("openid");

            if (StringUtils.isEmpty(openid)) {
                resultMap.put("state",200);
                resultMap.put("message", "未获取到openid");
                return resultMap;
            } else {
                User users = userMapper.getUserByOpenid(openid);
                // 判断是否为首次登陆
                if (users == null) {
                    // 首次登陆，创建用户信息
                    String time = TimeUtil.getCurrentTime("yyMMdd");

                    User user = new User(openid,null,"","","","",
                            "","");
                    userMapper.insertUser(user);

                    // 登陆的时候默认分配患者的角色
                    UserRole userRole = new UserRole(openid,1,"");
                    userRoleMapper.insertUserRole(userRole);

                    // 角色分配完成后，添加病人的角色
                    PatientDTO patientDTO = new PatientDTO(null,user.getPhoneNumber(),user.getIdcardNumber(),
                            user.getSex(),user.getBirth(),user.getLivingPlace(),user.getNation(),openid, null,
                            null,null,null, null,null);
                    patientService.editPatient(patientDTO);

                    resultMap.put("state", 0);
                    resultMap.put("openid", openid);
                } else {
                    resultMap.put("state", 200);
                    // 前台拿的数据是map的key
                    resultMap.put("openid", openid);
                    resultMap.put("user", users);
                }
        }
            response.close();
            client.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return resultMap;
    }
}
