package com.check.service.impl;

import com.check.base.Result;
import com.check.mapper.HistoryMapper;
import com.check.mapper.MentalStressMapper;
import com.check.mapper.UserMapper;
import com.check.pojo.History;
import com.check.pojo.MentalStress;
import com.check.pojo.TransObject;
import com.check.pojo.User;
import com.check.service.HistoryService;
import com.check.service.MentalStressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

@Service("mentalStressService")
@SuppressWarnings("all")
public class MentalStressServiceImpl implements MentalStressService {

    @Autowired
    private MentalStressMapper mentalStressMapper ;

    @Autowired
    private UserMapper userMapper ;

    @Autowired
    private HistoryMapper historyMapper;

    @Autowired
    private HistoryService historyService;

    @Override
    public Result<MentalStress> getMentalStressByTitleNum(String titleNum) {
        List<MentalStress> stress = mentalStressMapper.getMentalStressByTitleNum(titleNum);
        return Result.success(stress.size(),stress);
    }

    @Override
    public Result<MentalStress> getMentalStressByTitleNumAndOption(String titleNum, String option) {
        MentalStress stress = mentalStressMapper.getMentalStressByTitleNumAndOption(titleNum, option);
        return Result.success(stress);
    }

    @Override
    public Result<MentalStress> getMentalStressByMSId(Integer MSId) {
        MentalStress stress = mentalStressMapper.getMentalStressByMSId(MSId);
        return Result.success(stress);
    }

    @Override
    public Set<String> recommendScreeningProgramme(Map map) {

        // 提取map中的元素
        String openid = (String) map.get("openid");
        List<TransObject> list = (List<TransObject>) map.get("transObj");

        // 创建返回的结果结合，采用Set无重复结合进行收录
        Set<String> SPList = new HashSet<>();
        List<History> transList = new ArrayList<>();
        int hfId = 1 ;

        // 确定用户
        User user = userMapper.getUserByOpenid(openid);
        int birth = Integer.parseInt(user.getIdcardNumber().substring(6, 10));
        int year = Calendar.getInstance().getWeekYear();
        int age = year - birth;

        // 确定时间
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String fillinTime = sdf.format(date);
        System.out.println("fillinTime:" + fillinTime);

        Result<History> histories = historyService.getAllHistories();
        int size = histories.getDatas().size();
        if (size != 0) {
            hfId = histories.getDatas().get(size - 1).getHisId() + 1;
        }


        // 将所有的titleNum进行收集，去重
        Set<String> titleNums = new HashSet<>();
        for (TransObject transObject : list) {
            String titleNum = transObject.getTitleNum();
            titleNums.add(titleNum);
        }

        System.out.println("Health/smoke" + age);

        // 进行推荐
        // 找到每个题目内的每一个选项的值，进行查询推荐，然后返回推荐的结果
        for (TransObject transObject : list) {
            for (String titleNum : titleNums) {
                if (transObject.getTitleNum().equals(titleNum)) {
                    List<String> programme = mentalStressMapper.getScreeningProgramme(titleNum, transObject.getOptions(), age);
                    if (!programme.isEmpty()) {
                        for (String s : programme) {
                            // 如果结果不为空，那么添加到去重的Set集合里面去，数据库添加一条历史记录
                            if (!s.equals("")) {
                                SPList.add(s);

                                // 封装历史信息，提交到数据库
                                History history = new History(hfId, openid, s, "", 8);
                                System.out.println(history);
                                transList.add(history);
                                hfId++;
                            }
                        }
                    }
                }
            }
        }
        System.out.println(transList);

//        // 执行插入操作
        historyService.saveHistory(openid, "", 8, transList);

        // 执行更新操作
            // 1. 首次使用，根据openid查出用户的所有历史记录，全部更新
            // 2. 第二次及之后使用的时候，只需要查询出用户尚未填写的时间记录，更新时间即可
           // 综上所述，只要更新尚未填写的时间即可
        historyService.updateHistoryTime(openid,fillinTime);

        return SPList;
    }
}
