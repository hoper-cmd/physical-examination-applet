package com.check.service.impl;

import com.check.base.Result;
import com.check.mapper.HealthMenstrualMapper;
import com.check.mapper.UserMapper;
import com.check.pojo.HealthMenstrual;
import com.check.pojo.History;
import com.check.pojo.TransObject;
import com.check.pojo.User;
import com.check.service.HealthMenstrualService;
import com.check.service.HistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service("healthMenstrualService")
@SuppressWarnings("all")
public class HealthMenstrualServiceImpl implements HealthMenstrualService {

    @Autowired
    private HealthMenstrualMapper healthMenstrualMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private HistoryService historyService;


    @Override
    public Result<HealthMenstrual> getHealthMenstrualByTitleNum(String titleNum) {
        List<HealthMenstrual> menstrual = healthMenstrualMapper.getHealthMenstrualByTitleNum(titleNum);
        return Result.success(menstrual.size(),menstrual);
    }

    @Override
    public Result<HealthMenstrual> getHealthMenstrualByTitleNumAndOption(String titleNum, String option) {
        HealthMenstrual menstrual = healthMenstrualMapper.getHealthMenstrualByTitleNumAndOption(titleNum, option);
        return Result.success(menstrual);
    }

    @Override
    public Result<HealthMenstrual> getHealthMenstrualByHMId(Integer HMId) {
        HealthMenstrual menstrual = healthMenstrualMapper.getHealthMenstrualByHMId(HMId);
        return Result.success(menstrual);
    }

    @Override
    public Set<String> recommendScreeningProgramme(Map map) {

        // 提取map中的元素
        String openid = (String) map.get("openid");
        List<TransObject> list = (List<TransObject>) map.get("transObj");

        // 创建返回的结果结合，采用Set无重复结合进行收录
        Set<String> SPList = new HashSet<>();
        List<History> transList = new ArrayList<>();
        int hfId = 1 ;

        // 确定用户
        User user = userMapper.getUserByOpenid(openid);
        // 这条主要针对女性，男性数据传出过来不解析
        if(user.getSex().equals("女")) {
            int birth = Integer.parseInt(user.getIdcardNumber().substring(6, 10));
            int year = Calendar.getInstance().getWeekYear();
            int age = year - birth;

            Result<History> histories = historyService.getAllHistories();
            int size = histories.getDatas().size();
            if (size != 0) {
                hfId = histories.getDatas().get(size - 1).getHisId() + 1;
            }

            // 将所有的titleNum进行收集，去重
            Set<String> titleNums = new HashSet<>();
            for (TransObject transObject : list) {
                String titleNum = transObject.getTitleNum();
                titleNums.add(titleNum);
            }

            System.out.println("Health/menstrual" + age);

            // 进行推荐
            // 找到每个题目内的每一个选项的值，进行查询推荐，然后返回推荐的结果
            for (TransObject transObject : list) {
                for (String titleNum : titleNums) {
                    if (transObject.getTitleNum().equals(titleNum)) {
                        List<String> programme = healthMenstrualMapper.getScreeningProgramme(titleNum, transObject.getOptions(), age);
                        if (!programme.isEmpty()) {
                            for (String s : programme) {
                                // 如果结果不为空，那么添加到去重的Set集合里面去，数据库添加一条历史记录
                                if (!s.equals("")) {
                                    SPList.add(s);

                                    // 封装历史信息，提交到数据库
                                    History history = new History(hfId, openid, s, "", 5);
                                    System.out.println(history);
                                    transList.add(history);
                                    hfId++;
                                }
                            }
                        }
                    }
                }
            }

            System.out.println(transList);
//        // 执行插入操作
            historyService.saveHistory(openid, "", 5, transList);
        }
        return SPList;
    }
}
