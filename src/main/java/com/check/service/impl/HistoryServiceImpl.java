package com.check.service.impl;

import com.check.base.Result;
import com.check.mapper.HistoryMapper;
import com.check.pojo.History;
import com.check.service.HistoryService;
import com.check.util.HistoryUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("hFHistoryService")
@SuppressWarnings("all")
public class HistoryServiceImpl implements HistoryService {

    @Autowired
    private HistoryMapper historyMapper;

    @Override
    public Result<History> getAllHistories() {
        List<History> histories = historyMapper.getAllHistories();
        return Result.success(histories.size(),histories);
    }

    @Override
    public Result<History> getHistoryById(String openid) {
        List<History> historyById = historyMapper.getHistoryById(openid);
        return Result.success(historyById.size(), historyById);
    }

    @Override
    public Result<String> getResultsByOpenidAndFillInTime(String openid, String fillinTime) {
        List<String> histories = historyMapper.getResultsByOpenidAndFillinTime(openid, fillinTime);
        return Result.success(histories.size(), histories);
    }


    @Override
    public Result<History> getHistoryByTime(String fillinTime) {
        List<History> historyByTime = historyMapper.getHistoryByTime(fillinTime);
        return Result.success(historyByTime.size(),historyByTime);
    }

    @Override
    public Result<String> getTime(String openid) {
        List<String> time = historyMapper.getTime(openid);
        System.out.println(time);
        return Result.success(time.size(),time);
    }

    @Override
    public List<String> getTimeList(String openid) {
        return historyMapper.getTime(openid);
    }

    @Override
    public List<History> getHistoryByOpenidAndFillInTime(String openid, String fillinTime) {
        return historyMapper.getHistoryByOpenidAndFillInTime(openid, fillinTime);
    }

    @Override
    public Result<Integer> saveHistory(String openid, String fillinTime, Integer ids, List<History> list) {
        // 记录执行成功的条数
        int count = 0 ;
        List<History> history = historyMapper.getHistory(openid, fillinTime, ids);
        if (HistoryUtil.compare(list,history)){
            // 执行修改操作
            for (History history1 : list) {
                count +=  historyMapper.updateHistory(history1);
            }
        }else {
            // 执行插入操作
            for (History history1 : list) {
                // 如果没有相同项，执行插入。否则不插入
                if(!checkHasSame(history1)){
                    count += historyMapper.insertHistory(history1);
                }
            }
        }
        if (count > 0)
            return Result.success(count,list);
        return Result.failure();
    }

    @Override
    public Result<Integer> updateHistoryTime(String openid,String fillinTime) {
        // 记录执行成功的条数
        int count = 0 ;

        // 查询所有的记录
        List<History> histories = historyMapper.getHistoryByOpenidAndFillInTime(openid,"");

        // 逐条更新
        for (History history : histories) {
            History history1 = new History(history.getHisId(),history.getOpenid(),history.getResult(),fillinTime,history.getIds());
            int i = historyMapper.updateHistory(history1);
            count += i;
        }

        if(count > 0)
            return Result.success(count);
        return Result.failure();
    }

    @Override
    public Result<Integer> deleteHistory(String openid) {
        int result = historyMapper.deleteHistory(openid);
        if(result > 0)
            return Result.success();
        return Result.failure();
    }

    @Override
    public Result deleteHistoryByOpenidAndFillInTime(String openid, String fillinTime) {
        int result = historyMapper.deleteHistoryByOpenidAndFillInTime(openid, fillinTime);
        if(result > 0)
            return Result.success();
        return Result.failure();
    }

    private final boolean checkHasSame(History history){
        List<History> histories = historyMapper.getAllHistories();

        // 查询所有的历史记录，判断有无重复项，有返回true，没有返回false
        for (History history1 : histories) {
           if(history1.getOpenid().equals(history.getOpenid())){
               if(history1.getResult().equals(history.getResult())){
                   if(history1.getFillinTime().equals(history.getFillinTime())){
                       if(history1.getIds().equals(history.getIds())){
                           return true;
                       }
                   }
               }
           }
        }
        return false;
    }
}
