package com.check.service.impl;

import com.check.mapper.InspectionItemMapper;
import com.check.pojo.InspectionItem;
import com.check.service.InspectionItemService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (InspectionItem)表服务实现类
 *
 * @author makejava
 * @since 2022-05-10 15:27:34
 */
@Service("inspectionItemService")
public class InspectionItemServiceImpl implements InspectionItemService {
    @Resource
    private InspectionItemMapper inspectionItemMapper;

    @Override
    public List<InspectionItem> selectAllInspectionItem() {
        return inspectionItemMapper.selectAllInspectionItem();
    }

    @Override
    public List<InspectionItem> selectByInspectionId(Integer inspectionid) {
        return inspectionItemMapper.selectByInspectionId(inspectionid);
    }

}
