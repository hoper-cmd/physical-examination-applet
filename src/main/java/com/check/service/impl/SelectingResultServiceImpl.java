package com.check.service.impl;

import com.check.mapper.SelectingResultMapper;
import com.check.pojo.SelectingResult;
import com.check.service.SelectingResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SelectingResultServiceImpl implements SelectingResultService {
    @Autowired
    private SelectingResultMapper selectingResultMapper;

    @Override
    public List<SelectingResult> getRecommendProject(String screening) {
        return this.selectingResultMapper.getRecommendProject(screening);
    }
}
