package com.check.service.impl;

import com.check.mapper.PhysicalResultMapper;
import com.check.pojo.PhysicalResult;
import com.check.service.PhysicalResultService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (PhysicalResult)表服务实现类
 *
 * @author makejava
 * @since 2022-05-10 15:27:34
 */
@Service("physicalResultService")
public class PhysicalResultServiceImpl implements PhysicalResultService {
    @Resource
    private PhysicalResultMapper physicalResultMapper;


    @Override
    public int insertResort(PhysicalResult physicalResult) {
        return this.physicalResultMapper.insertReport(physicalResult);
    }

    @Override
    public List<Integer> getInspectionIds(Integer projectid) {
        return this.physicalResultMapper.getInspectionIds(projectid);
    }

    @Override
    public List<String> selectNormal(Integer projectid, Integer inspectionid) {
        return this.physicalResultMapper.selectNormal(projectid, inspectionid);
    }


}
