package com.check.service.impl;

import com.check.base.Result;
import com.check.mapper.*;
import com.check.pojo.*;
import com.check.pojo.dto.DoctorDTO;
import com.check.service.DoctorService;
import com.check.util.IdGenerateUtil;
import com.check.util.TimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.*;

@Service("doctorService")
public class DoctorServiceImpl implements DoctorService {

    @Autowired
    private DoctorMapper doctorMapper;

    @Autowired
    private BookScheduleMapper bookScheduleMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private ReservationMapper reservationMapper;

    @Autowired
    private PatientMapper patientMapper;

    @Autowired
    private UserRoleMapper userRoleMapper;

    @Autowired
    private MedicalRecordMapper medicalRecordMapper;

    @Override
    public List<String> getDoctorAccounts() {
        return userRoleMapper.getAccounts(2);
    }

    @Override
    public Map<String, Object> getDoctorInfo(String openid) {
        Doctor doctor = doctorMapper.getDoctorInfo(openid);

        Map<String,Object> map = new HashMap<>();
        map.put("doctorDepart",doctor.getHospitalDepartment());
        map.put("doctorSupports",doctor.getHospitalSupports());
        map.put("doctorName",doctor.getDoctorName());
        return map;
    }

    @Override
    public void insertBookSchedule(String openid, String bookDate, String time, Integer maxBookMember) {
        if(null == maxBookMember || 0 == maxBookMember){
            maxBookMember = 10;
        }
        BookSchedule bookSchedule = new BookSchedule();
        bookSchedule.setDoctorOpenid(openid);
        bookSchedule.setBookDate(bookDate);
        bookSchedule.setBookTime(time);
        bookSchedule.setMaxBookMember(maxBookMember);

        bookScheduleMapper.insertBookSchedule(bookSchedule);
    }

    @Override
    public List<Map<String, Object>> getReservationByDoctorOpenid(String doctorOpenid) {
       List<Map<String,Object>> list = new ArrayList<>();

        // 获取医生编号和姓名
        Doctor doctorInfo = doctorMapper.getDoctorInfo(doctorOpenid);
        String doctorNumber = doctorInfo.getDoctorNumber();
        String doctorName = doctorInfo.getDoctorName();
        String doctorTitle = doctorInfo.getDoctorTitle();
        String openid = doctorInfo.getDoctorOpenid();
        String doctorPhoneNumber = userMapper.getUserByOpenid(openid).getPhoneNumber();

        // 获取全部的预约单信息
       List<Reservation> reservationList = reservationMapper.getReservationsByDoctorOpenid(doctorNumber);
        // 遍历预约单，分别查询各自的预约单上的信息
        for (Reservation reservation : reservationList) {
            // 查询患者的信息
            String patientNumber = reservation.getPatientNumber();

            Patient patient = patientMapper.getPatientInfoByPatientNumber(patientNumber);
            String patientName = patient.getPatientName();
            String patientOpenid = patient.getPatientOpenid();
            String patientPhoneNumber = userMapper.getUserByOpenid(patientOpenid).getPhoneNumber();

            // 封装map
            Map<String,Object> map = new HashMap<>();
            map.put("reservationNumber",reservation.getReservationNumber());
            map.put("patientNumber",patientNumber);
            map.put("patientPhoneNumber",patientPhoneNumber);
            map.put("patientName",patientName);
            map.put("treatedCampus",reservation.getTreatedCampus());
            map.put("treatedDepartment",reservation.getTreatedDepartment());
            map.put("doctorName",doctorName);
            map.put("reservationStartTime",reservation.getReservationStartTime());
            map.put("doctorTitle", doctorTitle);
            map.put("doctorPhoneNumber",doctorPhoneNumber);

            list.add(map);
        }
        return list;
    }

    @Override
    public void deleteReservation(String reservationNumber) {
        // 获取当前的时间
        reservationMapper.deleteReservation(reservationNumber, TimeUtil.getCurrentDateAndTime());
    }

    @Override
    public List<Map<String, Object>> getAllDoctorsInfo() {
        List<Map<String,Object>> list = new ArrayList<>();
        List<Doctor> doctorList = doctorMapper.getAllDoctorInfo();
        for (Doctor doctor : doctorList) {
            // 医生账号，医生姓名，医生手机号码，医生身份证号，性别，医生职称
            String doctorOpenid = doctor.getDoctorOpenid();
            UserRole userRole =  userRoleMapper.getUserRoleInfo(doctorOpenid,2);
            String doctorAccount = userRole.getAccount();

            String doctorName = doctor.getDoctorName();
            String doctorTitle = doctor.getDoctorTitle();

            User user = userMapper.getUserByOpenid(doctorOpenid);
            String doctorPhoneNumber = user.getPhoneNumber();
            String idcardNumber = user.getIdcardNumber();
            String sex = user.getSex();

            Map<String,Object> map = new HashMap<>();
            map.put("doctorAccount",doctorAccount);
            map.put("doctorName",doctorName);
            map.put("doctorTitle",doctorTitle);
            map.put("doctorPhoneNumber",doctorPhoneNumber);
            map.put("idcardNumber",idcardNumber);
            map.put("sex",sex);

            list.add(map);
        }
        return list;
    }

    @Override
    public Result editDoctor(DoctorDTO doctorDTO) {
        UserRole userRole =  userRoleMapper.getInfoByAccount(doctorDTO.getDoctorAccount());
        String openid = userRole.getOpenid();

        User user = new User();
        user.setOpenid(openid);
        user.setRealName(doctorDTO.getRealName());
        user.setPhoneNumber(doctorDTO.getPhoneNumber());
        user.setIdcardNumber(doctorDTO.getIdcardNumber());
        user.setSex(doctorDTO.getSex());
        user.setBirth(doctorDTO.getBirth());
        user.setLivingPlace(doctorDTO.getLivingPlace());
        user.setNation(doctorDTO.getNation());
        int result1 = userMapper.updateUser(user);

        Doctor doctor = new Doctor();
        doctor.setDoctorName(doctorDTO.getDoctorName());
        doctor.setDoctorTitle(doctorDTO.getDoctorTitle());
        doctor.setDoctorOpenid(openid);
        doctor.setHospitalSupports(doctorDTO.getHospitalSupports());
        doctor.setHospitalDepartment(doctorDTO.getHospitalDepartment());

        // 判断是否有信息，没有做插入操作
        Doctor doctorInfo = doctorMapper.getDoctorInfo(openid);
        int result2 = 0 ;
        if(null == doctorInfo){
            List<Doctor> doctorList = doctorMapper.getAllDoctorInfo();
            int size = doctorList.size();
            int doctorId = 0;
            if(size != 0){
                doctorId = doctorList.get(size-1).getDoctorId();
            }
            doctor.setDoctorNumber(IdGenerateUtil.NumberGenerate(2,doctorId));
            result2 = doctorMapper.insertDoctor(doctor);
        }else {
            doctor.setDoctorNumber(doctorDTO.getDoctorNumber());
            result2 = doctorMapper.updateDoctor(doctor);
        }

        if(result1 + result2 >= 2){
            return Result.success(200,"修改成功");
        }else {
            return Result.failure(0,"修改失败，请联系运维人员");
        }
    }

    @Override
    public void insertBookScheduleDate(String doctorAccount, String[] bookDateList) {
        UserRole userRoleInfo = userRoleMapper.getInfoByAccount(doctorAccount);
        String doctorOpenid = userRoleInfo.getOpenid();

        for (String bookDate : bookDateList) {
            BookSchedule bookSchedule = new BookSchedule();
            bookSchedule.setBookDate(bookDate);
            bookSchedule.setDoctorOpenid(doctorOpenid);
            bookScheduleMapper.insertBookSchedule(bookSchedule);
        }
    }

    @Override
    public void insertBookScheduleTime(String doctorAccount, String bookDate, String[] timeList) {
        UserRole userRoleInfo = userRoleMapper.getInfoByAccount(doctorAccount);
        String doctorOpenid = userRoleInfo.getOpenid();

        // 第一个时间执行更新操作
        BookSchedule bookSchedule = new BookSchedule();
        bookSchedule.setBookDate(bookDate);
        bookSchedule.setDoctorOpenid(doctorOpenid);
        bookSchedule.setBookTime(timeList[0]);
        bookScheduleMapper.updateBookSchedule(bookSchedule);

        // 剩下的执行插入操作
        for (int i = 1; i < timeList.length ; i++) {
            BookSchedule bookSchedule1 = new BookSchedule();
            bookSchedule1.setBookDate(bookDate);
            bookSchedule1.setDoctorOpenid(doctorOpenid);
            bookSchedule1.setBookTime(timeList[i]);
            bookScheduleMapper.insertBookSchedule(bookSchedule1);
        }
    }

    @Override
    public List<String> getBookScheduleDateList(String doctorAccount) {
        UserRole userRoleInfo = userRoleMapper.getInfoByAccount(doctorAccount);
        String doctorOpenid = userRoleInfo.getOpenid();
        return doctorMapper.getBookScheduleDateList(doctorOpenid);
    }

    @Override
    public List<String> getBookScheduleTime(String doctorAccount, String bookDate) {
        UserRole userRoleInfo = userRoleMapper.getInfoByAccount(doctorAccount);
        String doctorOpenid = userRoleInfo.getOpenid();
        return doctorMapper.getBookScheduleTime(doctorOpenid,bookDate);
    }

    @Override
    public List<Map<String, Object>> getRecordsByDoctorAccount(String doctorAccount) {
        List<Map<String,Object>> list = new ArrayList<>();
        UserRole userRole = userRoleMapper.getInfoByAccount(doctorAccount);

        // 就诊院区，就诊科室，医生姓名
        String doctorOpenid = userRole.getOpenid();
        Doctor doctorInfo = doctorMapper.getDoctorInfo(doctorOpenid);
        String doctorNumber = doctorInfo.getDoctorNumber();
        String doctorName = doctorInfo.getDoctorName();
        String hospitalSupports = doctorInfo.getHospitalSupports();
        String hospitalDepartment = doctorInfo.getHospitalDepartment();

        List<MedicalRecord> medicalRecordList = medicalRecordMapper.getRecordsByDoctorNumber(doctorNumber);
        for (MedicalRecord medicalRecord : medicalRecordList) {
            String treatedEndedTime = medicalRecord.getTreatedEndedTime();
            if("".equals(treatedEndedTime) || null == treatedEndedTime){
                // 患者编号，患者手机号码，患者姓名
                String patientNumber = medicalRecord.getPatientNumber();
                Patient patientInfo = patientMapper.getPatientInfoByPatientNumber(patientNumber);
                String patientName = patientInfo.getPatientName();

                String patientOpenid = patientInfo.getPatientOpenid();
                String patientPhoneNumber = userMapper.getUserByOpenid(patientOpenid).getPhoneNumber();

                // 预约单号、预约时间
                String reservationNumber = medicalRecord.getReservationNumber();
                Reservation reservation = reservationMapper.getReservation(reservationNumber);
                String reservationStartTime = reservation.getReservationStartTime();

                Map<String,Object> map = new HashMap<>();
                map.put("reservationNumber",reservationNumber);
                map.put("patientNumber",patientNumber);
                map.put("patientPhoneNumber",patientPhoneNumber);
                map.put("patientName",patientName);
                map.put("hospitalSupports",hospitalSupports);
                map.put("hospitalDepartment",hospitalDepartment);
                map.put("doctorName",doctorName);
                map.put("reservationStartTime",reservationStartTime);

                list.add(map);
            }
        }
        return list;
    }

    @Override
    public void endThisReservation(String reservationNumber) {
        // 获取当前时间，如果预约时间晚于当前时间，取消预;反之，结束预约
        Reservation reservation = reservationMapper.getReservation(reservationNumber);
        String reservationStartTime = reservation.getReservationStartTime();
        boolean flag = judgeDateAndTime(reservationStartTime);
        if(flag){
            reservation.setIsCanceled(1);
        }else {
            reservation.setIsEnded(1);
        }
        String currentTimeWithoutSeconds = TimeUtil.getCurrentTimeWithoutSeconds();
        reservation.setReservationEndedTime(currentTimeWithoutSeconds);
        reservationMapper.updateReservation(reservation);

        // 同时更新记录表
        MedicalRecord medicalRecord  = medicalRecordMapper.getRecordsByReservationNumber(reservationNumber);
        medicalRecord.setTreatedEndedTime(currentTimeWithoutSeconds);
        medicalRecordMapper.updateMedicalRecord(medicalRecord);
    }

    @Override
    public Result showDoctorInfo(String doctorAccount) {
        UserRole userRole = userRoleMapper.getInfoByAccount(doctorAccount);
        String openid = userRole.getOpenid();
        User user = userMapper.getUserByOpenid(openid);
        Doctor doctorInfo = doctorMapper.getDoctorInfo(openid);
        if(doctorInfo != null){
            String realName = user.getRealName();
            String idcardNumber = user.getIdcardNumber();
            String phoneNumber = user.getPhoneNumber();
            String doctorTitle = doctorInfo.getDoctorTitle();
            String sex = user.getSex();
            String hospitalSupports = doctorInfo.getHospitalSupports();
            String hospitalDepartment = doctorInfo.getHospitalDepartment();

            Map<String,Object> map = new HashMap<>();
            map.put("realName",realName);
            map.put("idcardNumber",idcardNumber);
            map.put("phoneNumber",phoneNumber);
            map.put("doctorTitle",doctorTitle);
            map.put("sex",sex);
            map.put("hospitalSupports",hospitalSupports);
            map.put("hospitalDepartment",hospitalDepartment);

            return Result.success(200,"成功",map);
        }else {
            return Result.failure(0,"暂无该医生信息");
        }
    }

    /**
     * 时间字符串判断
     * @param str 字符串
     * @return true 前者大于后者（前者晚于后者） false-前者小于后者（前者早于后者）
     */
    private boolean judgeDateAndTime(String str){
        String year = str.substring(0, 4);
        String month = str.substring(5, 7);
        String date = str.substring(8, 10);
        String hour = str.substring(11,13);
        String minute = str.substring(14, 16);

        String currentYear = TimeUtil.getCurrentTime("yyyy");
        String currentMonth = TimeUtil.getCurrentTime("MM");
        String currentDate = TimeUtil.getCurrentTime("dd");
        String currentHour = TimeUtil.getCurrentTime("HH");
        String currentMinute = TimeUtil.getCurrentTime("mm");

        if(judge(year,currentYear))
            return true;
        else if(judge(currentYear,year))
            return false;
        else {
            if(judge(month,currentMonth))
                return true;
            else if(judge(currentMonth,month))
                return false;
            else {
                if (judge(date,currentDate))
                    return true;
                else if (judge(currentDate,date))
                    return false;
                else {
                    if (judge(hour,currentHour))
                        return true;
                    else if (judge(currentHour,hour))
                        return false;
                    else{
                        return judge(minute, currentMinute);
                    }
                }
            }

        }
    }

    /**
     * 字符串大小判断
     * @param str1 字符串1
     * @param str2 字符串2
     * @return true-晚于 false-早于
     */
    private boolean judge(String str1, String str2){
        int number1 = Integer.parseInt(str1);
        int number2 = Integer.parseInt(str2);
        return number1 > number2;
    }
}
