package com.check.service.impl;

import com.check.base.Result;
import com.check.mapper.HabitDietMapper;
import com.check.mapper.UserMapper;
import com.check.pojo.HabitDiet;
import com.check.pojo.History;
import com.check.pojo.TransObject;
import com.check.pojo.User;
import com.check.service.HabitDietService;
import com.check.service.HistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service("habitDietService")
@SuppressWarnings("all")
public class HabitDietServiceImpl implements HabitDietService {

    @Autowired
    private HabitDietMapper habitDietMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private HistoryService historyService;

    @Override
    public Result<HabitDiet> getHabitDietByTitleNum(String titleNum) {
        List<HabitDiet> diet = habitDietMapper.getHabitDietByTitleNum(titleNum);
        return Result.success(diet.size(),diet);
    }

    @Override
    public Result<HabitDiet> getHabitDietByTitleNumAndOption(String titleNum, String option) {
        HabitDiet diet = habitDietMapper.getHabitDietByTitleNumAndOption(titleNum, option);
        return Result.success(diet);
    }

    @Override
    public Result<HabitDiet> getHabitDietByHDId(Integer HDId) {
        HabitDiet diet = habitDietMapper.getHabitDietByHDId(HDId);
        return Result.success(diet);
    }

    @Override
    public Set<String> recommendScreeningProgramme(Map map) {

        // 提取map中的元素
        String openid = (String) map.get("openid");
        List<TransObject> list = (List<TransObject>) map.get("transObj");

        // 创建返回的结果结合，采用Set无重复结合进行收录
        Set<String> SPList = new HashSet<>();
        List<History> transList = new ArrayList<>();
        int hfId = 1 ;

        // 确定用户
        User user = userMapper.getUserByOpenid(openid);
        int birth = Integer.parseInt(user.getIdcardNumber().substring(6, 10));
        int year = Calendar.getInstance().getWeekYear();
        int age = year - birth;

        Result<History> histories = historyService.getAllHistories();
        int size = histories.getDatas().size();
        if (size != 0) {
            hfId = histories.getDatas().get(size - 1).getHisId() + 1;
        }

        // 将所有的titleNum进行收集，去重
        Set<String> titleNums = new HashSet<>();
        for (TransObject transObject : list) {
            String titleNum = transObject.getTitleNum();
            titleNums.add(titleNum);
        }

        System.out.println("Health/smoke" + age);

        // 进行推荐
        // 找到每个题目内的每一个选项的值，进行查询推荐，然后返回推荐的结果
        for (TransObject transObject : list) {
            for (String titleNum : titleNums) {
                if (transObject.getTitleNum().equals(titleNum)) {
                    List<String> programme = habitDietMapper.getScreeningProgramme(titleNum, transObject.getOptions(), age);
                    if (!programme.isEmpty()) {
                        for (String s : programme) {
                            // 如果结果不为空，那么添加到去重的Set集合里面去，数据库添加一条历史记录
                            if (!s.equals("")) {
                                SPList.add(s);

                                // 封装历史信息，提交到数据库
                                History history = new History(hfId, openid, s, "", 6);
                                System.out.println(history);
                                transList.add(history);
                                hfId++;
                            }
                        }
                    }
                }
            }
        }
        System.out.println(transList);

//        // 执行插入操作
        historyService.saveHistory(openid, "", 6, transList);
        return SPList;
    }
}
