package com.check.service.impl;

import com.check.base.Result;
import com.check.mapper.HealthyFamilyMapper;
import com.check.mapper.UserMapper;
import com.check.pojo.HealthyFamily;
import com.check.pojo.History;
import com.check.pojo.TransObject;
import com.check.pojo.User;
import com.check.service.HealthyFamilyService;
import com.check.service.HistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service("healthyFamilyService")
@SuppressWarnings("all")
public class HealthyFamilyServiceImpl implements HealthyFamilyService {

    @Autowired
    private HealthyFamilyMapper healthyFamilyMapper ;

    @Autowired
    private UserMapper userMapper ;

    @Autowired
    private HistoryService historyService;

    @Override
    public Result<HealthyFamily> getHealthyFamilyByTitleNum(String titleNum) {
        List<HealthyFamily> healthyFamilyByOption = healthyFamilyMapper.getHealthyFamilyByTitleNum(titleNum);
        return Result.success(healthyFamilyByOption.size(),healthyFamilyByOption);
    }

    @Override
    public Result getHealthyFamilyByTitleNumAndOption(String titleNum, String option) {
        HealthyFamily family = healthyFamilyMapper.getHealthyFamilyByTitleNumAndOption(titleNum, option);
        return Result.success(1,family);
    }

    @Override
    public Result getHealthyFamilyByHFId(Integer HFId) {
        HealthyFamily healthyFamilyByHFId = healthyFamilyMapper.getHealthyFamilyByHFId(HFId);
        return Result.success(1,healthyFamilyByHFId);
    }

    @Override
    public Set<String> recommendScreeningProgramme(List<TransObject> list, Integer isEarly, String openid){
        // 创建返回的结果结合，采用Set无重复结合进行收录
        Set<String> SPList = new HashSet<>();
        List<History> transList = new ArrayList<>();
        int hfId = 1 ;

        Result<History> histories = historyService.getAllHistories();
        int size = histories.getDatas().size();
        if(size != 0 ){
            hfId = histories.getDatas().get(size-1).getHisId() + 1;
        }

        // 确定用户
        User user = userMapper.getUserByOpenid(openid);
        int birth = Integer.parseInt(user.getIdcardNumber().substring(6,10));
        int year = Calendar.getInstance().getWeekYear();
        int age = year - birth;

        // 将所有的titleNum进行收集，去重
        Set<String> titleNums = new HashSet<>();
        for (TransObject transObject : list) {
            String titleNum = transObject.getTitleNum();
            titleNums.add(titleNum);
        }

        System.out.println("Health/family" + age);

        // 进行推荐
        // 找到每个题目内的每一个选项的值，进行查询推荐，然后返回推荐的结果
        for (TransObject transObject : list) {
            for (String titleNum : titleNums) {
                if(transObject.getTitleNum().equals(titleNum)){
                    List<String> programme = healthyFamilyMapper.getScreeningProgramme(titleNum, transObject.getOptions(),age , isEarly);
                    if(!programme.isEmpty()) {
                        for (String s : programme) {

                            // 如果结果不为空，那么添加到去重的Set集合里面去，数据库添加一条历史记录
                            if (!s.equals("")) {
                                SPList.add(s);

                                // 封装历史信息，提交到数据库
                                History history = new History(hfId, openid, s, "", 1);
                                System.out.println(history);
                                transList.add(history);
                                hfId++;
                            }
                        }
                    }
                }
            }
        }

        System.out.println(transList);
//        // 执行插入操作
        historyService.saveHistory(openid,"",1,transList);

        return SPList;
    }
}
