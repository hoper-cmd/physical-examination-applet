package com.check.service.impl;

import com.check.mapper.SelectProjectMapper;
import com.check.pojo.SelectProject;
import com.check.service.SelectProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * (SelectProject)表服务实现类
 *
 * @author makejava
 * @since 2022-05-09 18:26:51
 */
@Service("selectProjectService")
public class SelectProjectServiceImpl implements SelectProjectService {

    @Autowired
    private SelectProjectMapper selectProjectMapper;


    @Override
    public List<Integer> getProjectId(String openid, String paytime) {
        return this.selectProjectMapper.getProjectId(openid, paytime);
    }

    @Override
    public List<SelectProject> getProjectEntity(String openid, String paytime){
        return this.selectProjectMapper.getProjectEntity(openid, paytime);
    }

    @Override
    public List<SelectProject> selectInspectionItemByProject(String openid, String paytime){
        return selectProjectMapper.selectInspectionItemByProject(openid, paytime);
    }

    @Override
    public List<String> getPayTime(String openid) {
        return selectProjectMapper.getPayTime(openid);
    }

    @Override
    public int insertProjects(String projectname, String openid, String paytime) {
        return selectProjectMapper.insertProjects(projectname,openid,paytime);
    }
}
