package com.check.service;

import com.check.base.Result;
import com.check.pojo.HealthMenstrual;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.Set;

@Repository
public interface HealthMenstrualService {
    /**
     * 根据选项查询记录
     * @param titleNum 题目编号
     * @return 符合条件的选项集
     */
    Result<HealthMenstrual> getHealthMenstrualByTitleNum(@Param("titleNum") String titleNum);

    /**
     * 根据标题和选项确定唯一元素
     * @param titleNum 题目编号
     * @param option 选项内容
     * @return 符合条件的一条记录
     */
    Result<HealthMenstrual> getHealthMenstrualByTitleNumAndOption(@Param("titleNum") String titleNum,
                                                              @Param("option") String option);


    /**
     * 查询记录
     * @param HMId 表主键
     * @return 符合条件的一条记录
     */
    Result<HealthMenstrual> getHealthMenstrualByHMId(@Param("HMId") Integer HMId);

    /**
     * 获取体检推荐结果
     * @param map 携带参数信息的集
     * @return 体检推荐结果的去重集
     */
    Set<String> recommendScreeningProgramme(Map map);
}
