package com.check.service;

import com.check.pojo.InspectionItem;

import java.util.List;


/**
 * (InspectionItem)表服务接口
 *
 * @author makejava
 * @since 2022-05-10 15:27:34
 */
public interface InspectionItemService {

    /**
     * 获取全部检验项目
     *
     * @return
     */
    List<InspectionItem> selectAllInspectionItem();

    List<InspectionItem> selectByInspectionId(Integer inspectionid);

}
