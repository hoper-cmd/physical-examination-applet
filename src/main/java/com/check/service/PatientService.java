package com.check.service;

import com.check.base.Result;
import com.check.pojo.ResultProject;
import com.check.pojo.dto.PatientDTO;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface PatientService {

    /**
     * 获取当前有号的医生
     * @param currentTab 0-当日有号   1-全部有号信息
     * @return 医生姓名，医生职称，所属院区、所属科室、有号的日期，有号的时间,医生手机号码
     */
    List<Map<String, Object>> getAllBookDoctors(Integer currentTab);

    /**
     * 患者获取历史记录
     * @param patientOpenid 患者openid
     * @return 历史记录集合 (医生姓名，医生手机号，就诊院区，就诊科室，患者姓名，预约时间)
     */
    List<Map<String, Object>> getRecordsByPatientOpenid(String patientOpenid);

    /**
     * 获取所有患者的信息
     * @return 患者编号，患者姓名，患者手机号码，患者身份证号，性别，职业，民族，婚姻状况，受教育程度
     */
    List<Map<String, Object>> getAllPatientsInfo();

    /**
     * 编辑患者的信息以及基本信息
     * @param patientDTO  patient所有信息
     * @return
     */
    Result editPatient(PatientDTO patientDTO);

    /**
     * 新增预约单
     * @param patientOpenid 患者唯一标识
     * @param doctorPhoneNumber 医生手机号
     * @param bookDateAndTime 预约时间
     * @return
     */
    Result insertReservation(String patientOpenid, String doctorPhoneNumber, String bookDateAndTime);

    /**
     * 结束此次预约
     * @param reservationNumber 预约单号
     */
    void endThisReservation(String reservationNumber);

    /**
     *获取所有的结果集
     * @return com.check.pojo.ResultProject
     */
    List<ResultProject> selectAllResult();

    /**
     * 查询对应的项目
     * @param option_list 选项集
     * @return com.check.pojo.ResultProject
     */
    List<String> queryProjectByOptions(String[] option_list);

    /**
     * 返回结果集
     * @param checkProject 项目列表
     * @return map
     */
    Map<String, List> queryByppProject(String[] checkProject);

    /**
     * 展示用户信息
     * @param patientOpenid 患者标识
     * @return com.check.base.Result
     */
    Result showPatientInfo(String patientOpenid);
}
