package com.check.service;

import com.check.pojo.SelectProject;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * (SelectProject)表服务接口
 *
 * @author makejava
 * @since 2022-05-09 18:26:51
 */
@Repository
public interface SelectProjectService {

    List<Integer> getProjectId(String openid, String paytime);

    List<SelectProject> getProjectEntity(String openid, String paytime);

    List<SelectProject> selectInspectionItemByProject(String openid, String paytime);

    List<String> getPayTime(String openid);

    int insertProjects(String projectname, String openid, String paytime);
}
