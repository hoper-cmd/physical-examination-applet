package com.check.service;

import com.check.base.Result;
import com.check.pojo.HealthyFamily;
import com.check.pojo.TransObject;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface HealthyFamilyService {
    /**
     * 根据选项查询记录
     * @param titleNum 题目编号
     * @return 符合条件的选项集
     */
    Result<HealthyFamily> getHealthyFamilyByTitleNum(@Param("titleNum") String titleNum);

    /**
     * 根据标题和选项确定唯一元素
     * @param titleNum 题目编号
     * @param option 选项内容
     * @return 符合条件的一条记录
     */
    Result<HealthyFamily> getHealthyFamilyByTitleNumAndOption(@Param("titleNum") String titleNum,
                                                            @Param("option") String option);


    /**
     * 查询记录
     * @param HFId 表主键
     * @return 符合条件的一条记录
     */
    Result<HealthyFamily> getHealthyFamilyByHFId(@Param("HFId") Integer HFId);

    /**
     * 获取体检推荐结果
     * @param list 前台传送的选项信息
     * @param isEarly 是否早发
     * @param openid 微信用户唯一标识
     * @return 体检推荐结果的去重集
     */
    Set<String> recommendScreeningProgramme(List<TransObject> list, Integer isEarly, String openid);

}
