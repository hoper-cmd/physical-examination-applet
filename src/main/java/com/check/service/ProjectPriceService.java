package com.check.service;

import com.check.pojo.ProjectPrice;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface ProjectPriceService {


    ProjectPrice queryById(Integer ppId);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<ProjectPrice> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param projectPrice 实例对象
     * @return 实例对象
     */
    ProjectPrice insert(ProjectPrice projectPrice);

    /**
     * 修改数据
     *
     * @param projectPrice 实例对象
     * @return 实例对象
     */
    ProjectPrice update(ProjectPrice projectPrice);

    /**
     * 通过主键删除数据
     *
     * @param ppId 主键
     * @return 是否成功
     */
    boolean deleteById(Integer ppId);

    Map<String,List> queryByppProject(String[] ppProjects);

    ProjectPrice selectByppProject(String project);
}