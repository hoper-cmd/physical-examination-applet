package com.check.service;

import com.check.base.Result;
import com.check.pojo.Reservation;
import com.check.pojo.dto.CommandDTO;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface CommandService {
    /**
     * 获取所有未结束的预约单
     * @return 预约单号、患者编号，患者姓名，患者手机号码，就诊院区，就诊科室，预约医生，医生手机号码，医生职称
     */
    List<Map<String, Object>> getAllReservations();

    /**
     * 获取管理员账号集合
     * @return 账号集合
     */
    List<String> getCommandAccount();

    /**
     * 结束此次预约
     * @param reservationNumber 预约单号
     */
    void endThisReservation(String reservationNumber);

    /**
     * 修改预约单信息
     * @param reservation 预约单
     * @return >0-成功 0-失败
     */
    int editThisReservation(Reservation reservation);

    /**
     * 修改管理员个人信息
     * @param commandDTO com.check.pojo.CommandDTO
     * @return  com.base.Result
     */
    Result editPersonalInfo(CommandDTO commandDTO);

    /**
     * 展示管理端信息
     * @param commandAccount 管理员账户
     * @return com.check.pojo.User
     */
    Result showCommandInfo(String commandAccount);
}
