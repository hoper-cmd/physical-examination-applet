package com.check.service;

import com.check.base.Result;
import com.check.pojo.History;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HistoryService {

    /**
     * 获取全部的History信息
     * @return 返回集合
     */
    Result<History> getAllHistories();

    /**
     * 获取历史记录
     * @param openid 微信用户唯一标识
     * @return 符合条件的结果集
     */
    Result<History> getHistoryById(@Param("openid") String openid);

    /**
     * 获取调查问卷的结果
     * @param openid 微信用户唯一标识
     * @param fillinTime 填写时间
     * @return  调查问卷的结果集
     */
    Result<String> getResultsByOpenidAndFillInTime(@Param("openid") String openid,
                                                      @Param("fillinTime") String fillinTime);

    /**
     * 同时间内，产生的历史记录
     * @param fillinTime 填写时间
     * @return 符合条件的结果集
     */
    Result<History> getHistoryByTime(@Param("fillinTime") String fillinTime);

    /**
     * 获取时间列表
     * @param openid 微信用户唯一标识
     * @return 事件列表的String集合
     */
    Result<String> getTime(@Param("openid") String openid);

    /**
     * 获取时间列表
     * @param openid 微信用户唯一标识
     * @return 事件列表的String集合
     */
    List<String> getTimeList(@Param("openid") String openid);

    /**
     * 获取调查问卷的结果
     * @param openid 微信用户唯一标识
     * @param fillinTime 填写时间
     * @return  调查问卷的结果集
     */
    List<History> getHistoryByOpenidAndFillInTime(@Param("openid") String openid,
                                                  @Param("fillinTime") String fillinTime);

    /**
     * 爆粗你许改数据
     * @param openid 唯一标识
     * @param fillinTime dd
     * @param ids 组别码
     * @param list 插入时的History集合
     * @return 整型数字（>0 成功； <=0 失败）
     */
    Result<Integer> saveHistory(String openid, String fillinTime, Integer ids, List<History> list);


    /**
     * 更新用户填写的时间
     * @param openid openid
     * @param fillinTime 系统获取的填写完最后一个问卷的时间
     * @return 整型数字（>0 成功； <=0 失败）
     */
    Result<Integer> updateHistoryTime(@Param("openid") String openid,
                                      @Param("fillinTime") String fillinTime);


    /**
     * 删除一组记录
     * @param openid 微信用户身份唯一标识
     * @return 整型数字（>0 成功； <=0 失败）
     */
    Result<Integer> deleteHistory(@Param("openid") String openid);

    /**
     * 删除特定记录
     * @param openid 微信用户身份唯一标识
     * @param fillinTime 填写时间
     * @return 整型数字（>0 成功； <=0 失败）
     */
    Result deleteHistoryByOpenidAndFillInTime(@Param("openid") String openid,
                                             @Param("fillinTime") String fillinTime);
}
