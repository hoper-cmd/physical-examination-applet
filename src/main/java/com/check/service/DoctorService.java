package com.check.service;

import com.check.base.Result;
import com.check.pojo.dto.DoctorDTO;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface DoctorService {

    /**
     * 获取医生账号
     * @return 医生账号列表
     */
    List<String> getDoctorAccounts();

    /**
     * 获取医生信息
     * @param openid 唯一标识
     * @return 医生姓名、科室、院区
     */
    Map<String, Object> getDoctorInfo(String openid);

    /**
     * 新增预约信息
     * @param openid 唯一标识
     * @param bookDate 预约日期
     * @param time 预约时间
     * @param maxBookMember 最大预约人数
     */
    void insertBookSchedule(String openid, String bookDate, String time, Integer maxBookMember);

    /**
     * 获取预约单上的信息
     * @param doctorOpenid 唯一标识
     * @return  预约单号，患者编号，患者手机号码，患者姓名，就诊院区，就诊科室，医生姓名，预约时间
     */
    List<Map<String, Object>> getReservationByDoctorOpenid(String doctorOpenid);

    /**
     * 假删除预约单
     * @param reservationNumber 预约单号
     */
    void deleteReservation(String reservationNumber);

    /**
     * 获取所有的医生的信息
     * @return 医生账号，医生姓名，医生手机号码，医生身份证号，性别，医生职称
     */
    List<Map<String, Object>> getAllDoctorsInfo();

    /**
     * 修改Doctor以及基本信息
     * @param doctorDTO doctor所有信息
     * @return
     */
    Result editDoctor(DoctorDTO doctorDTO);

    /**
     * 增加预约日期
     * @param doctorAccount 医生账号
     * @param bookDateList 预约日期列表
     */
    void insertBookScheduleDate(String doctorAccount, String[] bookDateList);

    /**
     * 增加预约时间
     * @param doctorAccount 医生账号
     * @param bookDate 预约日期
     * @param timeList 日期下的时间列表
     */
    void insertBookScheduleTime(String doctorAccount,String bookDate, String[] timeList);

    /**
     * 获取预约设置的日期列表
     * @param doctorAccount 医生账户
     * @return 日期列表
     */
    List<String> getBookScheduleDateList(String doctorAccount);

    /**
     * 获取预约设置的时间列表
     * @param doctorAccount 医生账户
     * @param bookDate 预约日期
     * @return 时间列表
     */
    List<String> getBookScheduleTime(String doctorAccount, String bookDate);

    /**
     * 获取预约单记录
     * @param doctorAccount 医生账户
     * @return 预约单号，患者编号，患者手机号码，患者姓名，就诊院区，就诊科室，医生姓名，预约时间
     */
    List<Map<String, Object>> getRecordsByDoctorAccount(String doctorAccount);

    /**
     * 医生端结束此次预约
     * @param reservationNumber 预约单号
     */
    void endThisReservation(String reservationNumber);

    /**
     * 查看医生信息
     * @param doctorAccount 医生账号
     * @return com.check.pojo.Doctor
     */
    Result showDoctorInfo(String doctorAccount);
}
