package com.check.service;

import com.check.pojo.SelectingResult;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface SelectingResultService {

    List<SelectingResult> getRecommendProject(String screening);
}
