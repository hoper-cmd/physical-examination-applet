package com.check.util;

import com.check.pojo.TransObject;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class HistoryUtil {

    public static boolean compare(List<?> list1, List<?> list2){
        if(list1.size() == list2.size()){
            for (int i = 0; i < list1.size(); i++) {
                if(!list1.get(i).equals(list2.get(i)))
                    return false;
            }
            return true;
        }
        return false;
    }

    public static List setToList(Set set){
        List<Object> list = new ArrayList<>();
        for (Object o : set) {
            list.add(o);
        }
        return list;
    }

    public static Set listToSet(List list){
        Set<Object> set = new HashSet<>();
        for (Object o : list) {
            set.add(o);
        }
        return set;
    }

    public static BigDecimal dateToStamp(String s) {
        //设置时间模版
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = simpleDateFormat.parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long ts = date.getTime();
        BigDecimal res = BigDecimal.valueOf(ts);
        return res;
    }


    public static void main(String[] args) {
        List<TransObject> list1 = new ArrayList<>();
        list1.add(new TransObject("1-1","A"));
        list1.add(new TransObject("1-1","B"));
        list1.add(new TransObject("1-1","C"));

        List<TransObject> list2 = new ArrayList<>();
        list2.add(new TransObject("1-1","A"));
        list2.add(new TransObject("1-1","B"));
        list2.add(new TransObject("1-1","C"));

        System.out.println(compare(list1, list2));
    }
}
