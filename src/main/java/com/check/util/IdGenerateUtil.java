package com.check.util;

import java.util.UUID;

public class IdGenerateUtil {

    public static String IdGenerate(){
        // 3位前缀 + 日期(6位) + 3位的UUID
        String pre = String.valueOf(System.currentTimeMillis()).substring(7,10);
        String time = TimeUtil.getCurrentTime("yyyyMMdd");
        String uuid = UUID.randomUUID().toString().substring(0,3);
        return (pre+time+uuid);
    }

    /**
     * 生成编号
     * @param role 角色/身份
     * @return 医生：D+8位UUID+表的上一位id
     */
    public static String NumberGenerate(Integer role, Integer id){
        String pre = "";
        switch (role){
            case 2:
                pre += 'D';
                break;
            case 1:
                pre += 'P';
                break;
            default:
                return pre;
        }
        int length = String.valueOf(id).length();
        int necessaryUUIDLength = 8 - length;
        int beginIndex = 24;
        int endIndex = 24 + necessaryUUIDLength + 1;

        String uuid = UUID.randomUUID().toString().substring(beginIndex,endIndex);
        return (pre + uuid + id);
    }
}
