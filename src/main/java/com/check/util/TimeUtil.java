package com.check.util;

import java.text.SimpleDateFormat;

public class TimeUtil {
    public static String getCurrentDateAndTime(){
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return df.format(System.currentTimeMillis());
    }

    public static String getCurrentDateAndTimeWithoutSeconds(){
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return df.format(System.currentTimeMillis());
    }

    public static String getCurrentDate(){
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(System.currentTimeMillis());
    }

    public static String getCurrentTime(){
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        return df.format(System.currentTimeMillis());
    }

    public static String getCurrentTimeWithoutSeconds(){
        SimpleDateFormat df = new SimpleDateFormat("HH:mm");
        return df.format(System.currentTimeMillis());
    }

    public static String getCurrentTime(String pattern){
        SimpleDateFormat df = new SimpleDateFormat(pattern);
        return df.format(System.currentTimeMillis());
    }
}
