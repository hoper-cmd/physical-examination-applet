package com.check.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.check.pojo.SelectObject;
import com.check.pojo.dto.TransObjectDTO;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JSONUtil {
    public static Map JSON2Map(@RequestBody JSONObject transObject){

        String string = JSON.parseObject(String.valueOf(transObject), Map.class).get("transObject").toString();
        TransObjectDTO object = JSON.parseObject(string, TransObjectDTO.class);

        System.err.println("util-object-openid" + object.getOpenid());
        System.err.println("util-object-list" + object.getTransObj());

        Map map = new HashMap();
        map.put("openid", object.getOpenid());
        map.put("transObj", object.getTransObj());
        System.out.println(map);
        System.err.println("util-map-openid" + map.get("openid"));
        System.err.println("util-map-openid" + map.get("transObj"));


        return map;
    }

    public static SelectObject JSON2(@RequestBody JSONObject tranObject){

        SelectObject object = tranObject.parseObject(String.valueOf(tranObject), SelectObject.class);
        String openid = object.getOpenid();
        List<String> tranObj = object.getTranObj();
        return object;
    }
}
