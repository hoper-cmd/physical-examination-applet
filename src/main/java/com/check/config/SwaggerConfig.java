package com.check.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket docket(){
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .groupName("zzy")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.check.controller"))
                .build();
    }


    // 个人信息
    private ApiInfo apiInfo(){

        Contact contact = new Contact("","","");

        return new ApiInfo("查体小程序接口文档",
                "",
                "1.0",
                "Api",
                contact,
                "",
                "",
                new ArrayList<>());
    }
}
