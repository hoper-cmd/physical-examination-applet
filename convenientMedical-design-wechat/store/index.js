import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
	// state中是需要管理的全局变量
	state: {
		userInfo: {
			openid: '',
			avatarUrl: '',
			roles: 0
		},
		doctorAccount: '',
		administratorAccount: '',
		nickName: '',
		// 进度条的全局变量
		progress:0,
		// 判断填写完毕后，是不是进行了二次提交操作，若是二次提交操作我们就不对进度条进行修改操作
		// 在二次提交时我们要提醒用户此模块已经填写完毕，不必再次填写，如果想修改，可以进行二次提交操作
		access_basic:false,
		access_fam:false,
		access_pre:false,
		access_allergic:false,
		access_medication:false,
		access_operation:false,
		access_menstrual:false,
		access_physical_sym:false,
		access_diet:false,
		access_cigarette:false,
		access_wine:false,
		access_exercise:false,
		access_environment:false,
		access_mental:false,
		access_sleep:false,
		access_literacy:false,
		// 全局tabbar设置
		status1:0,
		status2:0,
		status3:0,
		status4:0,
		status5:0,
		status6:0,
		status7:0,
		status8:0,
		status9:0,
		status10:0,
		status11:0,
		status12:0
	},
	// mutations 是操作state中变量的方法
	mutations: {
		loginout(state) { //退出登录修改 全局变量
			state.userInfo.openid = '';
			state.userInfo.roles=0;
		},
		setDoctorAccount(state,newDoctorAccount){
			state.doctorAccount=newDoctorAccount
		},
		setAdministrator(state,newAdministratorAccount){
			state.administratorAccount=newAdministratorAccount
		},
		setStatus1(state){
			state.status1=1;
			state.status2=0;
			state.status3=0;
			state.status4=0;
		},
		setStatus2(state){
			state.status1=0;
			state.status2=1;
			state.status3=0;
			state.status4=0;
		},
		setStatus3(state){
			state.status1=0;
			state.status2=0;
			state.status3=1;
			state.status4=0;
		},
		setStatus4(state){
			state.status1=0;
			state.status2=0;
			state.status3=0;
			state.status4=1;
		},
		setStatus5(state){
			state.status5=1;
			state.status6=0;
			state.status7=0;
			state.status8=0;
		},
		setStatus6(state){
			state.status5=0;
			state.status6=1;
			state.status7=0;
			state.status8=0;
		},
		setStatus7(state){
			state.status5=0;
			state.status6=0;
			state.status7=1;
			state.status8=0;
		},
		setStatus8(state){
			state.status5=0;
			state.status6=0;
			state.status7=0;
			state.status8=1;
		},
		setStatus9(state){
			state.status9=1;
			state.status10=0;
			state.status11=0;
			state.status12=0;
		},
		setStatus10(state){
			state.status9=0;
			state.status10=1;
			state.status11=0;
			state.status12=0;
		},
		setStatus11(state){
			state.status9=0;
			state.status10=0;
			state.status11=1;
			state.status12=0;
		},
		setStatus12(state){
			state.status9=0;
			state.status10=0;
			state.status11=0;
			state.status12=1;
		},
		
		// 对start和end进行动态操作
		adddata(state){
			state.start+=10;
			state.end+=10;
		},
		reducedata(state){
			state.start-=10;
			state.end-=10;
		},
		
		
		// 以下方法用来判断是不是二次进入该模块，而不影响进度条的变化
		setProgress(state){
			state.progress+=6.25;
		},
		reduceProgress(state){
			state.progress-=6.25
		},
		setaccess_basic(state){
			state.access_basic=true
		},
		setaccess_fam(state){
			state.access_fam=true
		},
		setaccess_pre(state){
			state.access_pre=true
		},
		setaccess_allergic(state){
			state.access_allergic=true
		},
		setaccess_medication(state){
			state.access_medication=true;
		},
		setaccess_operation(state){
			state.access_operation=true;
		},
		setaccess_menstrual(state){
			state.access_menstrual=true;
		},
		setaccess_physical_sym(state){
			state.access_physical_sym=true;
		},
		setaccess_diet(state){
			state.access_diet=true;
		},
		setaccess_cigarette(state){
			state.access_cigarette=true;
		},
		setaccess_wine(state){
			state.access_wine=true;
		},
		setaccess_exercise(state){
			state.access_exercise=true;
		},
		
		setaccess_environment(state){
			state.access_environment=true;
		},
		
		setaccess_mental(state){
			state.access_mental=true;
		},
		
		setaccess_sleep(state){
			state.access_sleep=true;
		},
		
		setaccess_literacy(state){
			state.access_literacy=true;
		},
		setUserRole(state,role){
			state.userInfo.roles=role;
		},
		setNn(state, nickName) {
			state.nickName = nickName;
		},
		saveUserInfo(state, info) { //保存数据到 store-全局变量
			state.userInfo.openid = info.openid;
			state.userInfo.avatarUrl=info.avatarUrl;
			state.hasLogin = true;
		},

	}
})

export default store
